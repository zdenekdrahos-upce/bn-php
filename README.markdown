# BN-PHP

## About

* Big Number data type for PHP.
* Data type which can be used as replacement of inaccurate float
* Use [BC Math](http://www.php.net/manual/en/ref.bc.php).
* Included Math **expressions** parser for [infix notation](http://en.wikipedia.org/wiki/Infix_notation).
* Requirements: **PHP >= 5.3**.
* [Class API](http://zdenekdrahos.bitbucket.org/bn-php/index.html)

## Example usage

In PHP or any other programming language [floating point precision](http://php.net/manual/en/language.types.float.php) is a problem. For example result of operator `floor((0.1+0.7)*10)` is 7 instead of the expected 8. **BN-PHP always returns 8**.

![BN-PHP vs eval](http://zdenekdrahos.bitbucket.org/bn-php/floor-demo.png "BN-PHP vs eval")

### Basic usage of `BN\Number` data type

    $a = new BN\Number('0.1');
    $b = new BN\Number('0.7');
    $ten = new BN\Number('10');
    echo $a->add($b)->multiply($ten)->roundDown();

### Expression evaluator in action

    $evaluator('floor((0.1+0.7)*10)', $variables, $responder); 

For more examples and documentation check [wiki](https://bitbucket.org/zdenekdrahos/bn-php/wiki/).

## Facts

License: [New BSD License](https://bitbucket.org/zdenekdrahos/bn-php/src/tip/license.txt).

Author: [Zdenek Drahos](https://bitbucket.org/zdenekdrahos).

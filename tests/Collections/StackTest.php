<?php

namespace BN\Collections;

class StackTest extends \PHPUnit_Framework_TestCase
{
    /** @var Stack */
    private $emptyStack;
    /** @var Stack */
    private $nonEmptyStack;

    protected function setUp()
    {
        $this->emptyStack = new Stack();
        $this->nonEmptyStack = new Stack();
        $this->nonEmptyStack->push(1);
        $this->nonEmptyStack->push(2);
        $this->nonEmptyStack->push(3);
    }

    public function testClear()
    {
        $this->nonEmptyStack->clear();
        parent::assertSame(0, $this->nonEmptyStack->size());
    }

    public function testIsEmpty()
    {
        parent::assertTrue($this->emptyStack->isEmpty());
        parent::assertFalse($this->nonEmptyStack->isEmpty());
    }

    public function testSize()
    {
        parent::assertSame(0, $this->emptyStack->size());
        parent::assertTrue($this->nonEmptyStack->size() > 0);
    }

    public function testPush()
    {
        parent::assertSame(0, $this->emptyStack->size());
        $this->emptyStack->push(1);
        parent::assertSame(1, $this->emptyStack->size());

        parent::setExpectedException('\BN\Collections\NullArgumentException');
        $this->emptyStack->push(null);
        parent::assertSame(1, $this->emptyStack->size());
    }

    public function testPop()
    {
        parent::assertNull($this->emptyStack->pop());
        $currentSize = $this->nonEmptyStack->size();
        parent::assertSame(3, $this->nonEmptyStack->pop());
        parent::assertSame($currentSize - 1, $this->nonEmptyStack->size());
    }

    public function testPeek()
    {
        parent::assertNull($this->emptyStack->peek());
        $currentSize = $this->nonEmptyStack->size();
        parent::assertSame(3, $this->nonEmptyStack->peek());
        parent::assertSame($currentSize, $this->nonEmptyStack->size());
    }
}

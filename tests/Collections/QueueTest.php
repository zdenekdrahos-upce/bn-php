<?php

namespace BN\Collections;

class QueueTest extends \PHPUnit_Framework_TestCase
{
    /** @var Queue */
    private $emptyQueue;
    /** @var Queue */
    private $nonEmptyQueue;

    protected function setUp()
    {
        $this->emptyQueue = new Queue();
        $this->nonEmptyQueue = new Queue();
        $this->nonEmptyQueue->push(1);
        $this->nonEmptyQueue->push(2);
        $this->nonEmptyQueue->push(3);
    }

    public function testClear()
    {
        $this->nonEmptyQueue->clear();
        parent::assertSame(0, $this->nonEmptyQueue->size());
    }

    public function testIsEmpty()
    {
        parent::assertTrue($this->emptyQueue->isEmpty());
        parent::assertFalse($this->nonEmptyQueue->isEmpty());
    }

    public function testSize()
    {
        parent::assertSame(0, $this->emptyQueue->size());
        parent::assertTrue($this->nonEmptyQueue->size() > 0);
    }

    public function testPush()
    {
        parent::assertSame(0, $this->emptyQueue->size());
        $this->emptyQueue->push(1);
        parent::assertSame(1, $this->emptyQueue->size());

        parent::setExpectedException('\BN\Collections\NullArgumentException');
        $this->emptyQueue->push(null);
        parent::assertSame(1, $this->emptyQueue->size());
    }

    public function testPop()
    {
        parent::assertNull($this->emptyQueue->pop());
        $currentSize = $this->nonEmptyQueue->size();
        parent::assertSame(1, $this->nonEmptyQueue->pop());
        parent::assertSame($currentSize - 1, $this->nonEmptyQueue->size());
    }

    public function testPeek()
    {
        parent::assertNull($this->emptyQueue->peek());
        $currentSize = $this->nonEmptyQueue->size();
        parent::assertSame(1, $this->nonEmptyQueue->peek());
        parent::assertSame($currentSize, $this->nonEmptyQueue->size());
    }

    public function testClone()
    {
        $clone = clone $this->nonEmptyQueue;
        parent::assertTrue($clone->size() == $this->nonEmptyQueue->size());
        $clone->pop();
        parent::assertFalse($clone->size() == $this->nonEmptyQueue->size());
    }
}

<?php

namespace BN\Compiler\Scanner;

use Tests\BaseUnitTestCase;
use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;

class LexemeToTokenTest extends BaseUnitTestCase
{
    private $converter;

    protected function setUp()
    {
        BaseUnitTestCase::setUp();
        $unknown = new Converter\LexemeToUnknown();
        $this->converter = new LexemeToToken($unknown);
    }

    public function testWhenNoLexerCanConvertLexemeShouldReturnUnknownToken()
    {
        $this->assertTokenHasType('1250', TokenType::UNKNOWN);
    }

    public function testShouldConvertFirstConvertableLexeme()
    {
        $this->whenNumberCanBeConverted();
        $this->assertTokenHasType('1250', TokenType::NUMBER);
        $this->assertTokenHasType('1250anothertest', TokenType::NUMBER);
    }

    public function testShouldUseLexerWhichReadMoreCharacters()
    {
        $this->whenLexerIsRegistered('firstType', 'A');
        $this->whenLexerIsRegistered('secondType', 'AA');
        $this->assertTokenHasType('AA', 'secondType');
    }

    public function testShouldUseFirstLexerWhichCanConvertLexeme()
    {
        $this->whenLexerIsRegistered('firstType', 'A');
        $this->whenLexerIsRegistered('secondType', 'A');
        $this->assertTokenHasType('A', 'firstType');
    }

    private function whenNumberCanBeConverted()
    {
        $this->converter->registerLexer(new Converter\LexemeToUnsignedNumber());
    }

    private function whenLexerIsRegistered($type, $tokenValue)
    {
        $lexer = $this->mockista->create(
            'BN\Compiler\Scanner\LexemeConverter',
            array(
                'canConvertLexeme' => function ($lexeme) use ($tokenValue) {
                    return strlen($lexeme) == strlen($tokenValue);
                },
                'convertLexeme' => new Token($type, $tokenValue)
            )
        );
        $this->converter->registerLexer($lexer);
    }

    private function assertTokenHasType($lexeme, $expectedType)
    {
        $token = $this->converter->lexemeToToken($lexeme);
        assertThat($token->type, is($expectedType));
    }
}

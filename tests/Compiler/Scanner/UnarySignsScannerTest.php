<?php

namespace BN\Compiler\Scanner;

use BN\Compiler\Token\TokenType;

class UnarySignsScannerTest extends \PHPUnit_Framework_TestCase
{
    private $signs = array('-', '+');
    private $operators = array(
        '+' => TokenType::OPERATOR,
        '-' => TokenType::OPERATOR
    );
    private $tester;

    protected function setUp()
    {
        $this->tester = new \Tests\ScannerTester(
            array(
                'keywords' => $this->operators,
                'toNumber' => new Converter\LexemeToSignedNumber(),
                'scanner' => function (Scanner $scanner) {
                    return new UnarySignsScanner($scanner, $this->signs);
                },
                'statementsComparer' => function ($input, $scannedStatement) {
                    // spaces are added arounds signs
                    assertThat(strlen($scannedStatement), greaterThanOrEqualTo(strlen($input)));
                }
            )
        );
    }

    /** @dataProvider provideNumberWithSigns */
    public function testShouldFindNumberWithSign($numberWithSign)
    {
        $this->tester->shouldFindOneStatement(
            $numberWithSign,
            array($numberWithSign => TokenType::NUMBER)
        );
    }

    public function provideNumberWithSigns()
    {
        return array(
            array('-1'),
            array('+1')
        );
    }

    /** @dataProvider provideNumberOperatorNumber */
    public function testShouldSeparateUnaryAndBinarySign($expression, array $expectedTokens)
    {
        $this->tester->shouldFindOneStatement(
            $expression,
            array(
                $expectedTokens[0] => TokenType::NUMBER,
                $expectedTokens[1] => TokenType::OPERATOR,
                $expectedTokens[2] => TokenType::NUMBER,
            )
        );
    }

    public function provideNumberOperatorNumber()
    {
        return array(
            array('+1+-1', array('+1', '+', '-1')),
            array('-1-+1', array('-1', '-', '+1')),
            array('1+2', array('1', '+', '2')),
            array('1-2', array('1', '-', '2')),
        );
    }
}

<?php

namespace BN\Compiler\Scanner;

use BN\Compiler\Token\TokenType;

class ScannerTest extends \PHPUnit_Framework_TestCase
{
    private $keywords = array(
        '+' => TokenType::OPERATOR,
        'plus' => TokenType::OPERATOR,
    );
    private $tester;

    protected function setUp()
    {
        $this->tester = new \Tests\ScannerTester(
            array(
                'keywords' => $this->keywords,
                'toNumber' => new Converter\LexemeToUnsignedNumber(),
                'scanner' => function (Scanner $basicScanner) {
                    return $basicScanner;
                },
                'statementsComparer' => function ($input, $scannedStatement) {
                    assertThat($scannedStatement, is($input));
                }
            )
        );
    }

    public function testShouldReturnNoTokensForEmptyInput()
    {
        $this->tester->assertStatementsCount('', 0);
    }

    /** @dataProvider provideInputWithOneToken */
    public function testShouldFindOneToken($value, $expectedType)
    {
        $input = "{$value}";
        $this->tester->shouldFindOneToken($input, $value, $expectedType);
    }

    public function provideInputWithOneToken()
    {
        return array(
            'unknown' => array('UNDEFINED', TokenType::UNKNOWN),
            'number' => array('121', TokenType::NUMBER),
            'char operator' => array('+', TokenType::OPERATOR),
            'string operator' => array('plus', TokenType::OPERATOR),
        );
    }

    /** @dataProvider provideInputsNumberOneSurroundedByWhitespace */
    public function testShouldIgnoreWhitespaceAndScanOnlyNumberOne($input)
    {
        $this->tester->shouldFindOneToken($input, 1, TokenType::NUMBER);
    }

    public function provideInputsNumberOneSurroundedByWhitespace()
    {
        return array(
            'spaces' => array('  1   '),
            'whitespace' => array("\n1\n")
        );
    }

    /** @dataProvider provideOnePlusTwoWithWhitespace */
    public function testShouldSeparateNumbersAndKeyword($input, $plusOperator = '+')
    {
        $this->tester->shouldFindOneStatement($input, array(
            1 => TokenType::NUMBER,
            $plusOperator => TokenType::OPERATOR,
            2 => TokenType::NUMBER
        ));
    }

    public function provideOnePlusTwoWithWhitespace()
    {
        return array(
            // operator as 1 character
            'char - spaces' => array('1 + 2', '+'),
            'char - no space between' => array('1+2', '+'),
            'char - whitespace' => array("\n1\n+\n2", '+'),
            // string operator
            'string - spaces' => array('1 plus 2', 'plus'),
            'string - no space between' => array('1plus2', 'plus'),
            'string - whitespace' => array("\n1\nplus\n2\n", 'plus'),
        );
    }

    public function testShouldSeparateStatements()
    {
        $this->tester->assertStatementsCount('5;5;', 2);
    }
}

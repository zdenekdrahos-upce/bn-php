<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\TokenType;

class LexemeToConstantTest extends WhenLexemeIsConverted
{
    private $constants = array('numberOfTheBeast');

    protected function getConverter()
    {
        return new LexemeToConstant($this->constants);
    }

    protected function getTokenType()
    {
        return TokenType::CONSTANT;
    }

    public function provideInvalidLexemes()
    {
        return array(
            'undefined constant' => array('undefined'),
            'case sensitive' => array('NUMBEROfTheBeast'),
        );
    }

    public function provideValidLexemes()
    {
        return array(
            'defined constant' => array('numberOfTheBeast')
        );
    }

    protected function getTokenValue($name)
    {
        return $this->constants[$name];
    }
}

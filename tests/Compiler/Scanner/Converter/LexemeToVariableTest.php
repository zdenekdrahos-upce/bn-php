<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\TokenType;

class LexemeToVariableTest extends WhenLexemeIsConverted
{
    protected function getConverter()
    {
        return new LexemeToVariable();
    }

    protected function getTokenType()
    {
        return TokenType::VARIABLE;
    }

    public function provideInvalidLexemes()
    {
        return array(
            'no dollar' => array('variable'),
            'only alphabetic character' => array('$var2')
        );
    }

    public function provideValidLexemes()
    {
        return array(
            array('$variable')
        );
    }
}

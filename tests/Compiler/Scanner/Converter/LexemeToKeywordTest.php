<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\TokenType;

class LexemeToKeywordTest extends WhenLexemeIsConverted
{
    protected function getConverter()
    {
        $keywords = array(
            '+' => TokenType::OPERATOR,
            '*' => TokenType::OPERATOR
        );
        return new LexemeToKeyword($keywords);
    }

    protected function getTokenType()
    {
        return TokenType::OPERATOR;
    }

    public function provideInvalidLexemes()
    {
        return array(
            'undefined operator' => array('undefined')
        );
    }

    public function provideValidLexemes()
    {
        return array(
            'plus' => array('+'),
            'multiply' => array('*')
        );
    }
}

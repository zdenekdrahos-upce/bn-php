<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\TokenType;

class LexemeToUnknownTest extends WhenLexemeIsConverted
{
    protected function getConverter()
    {
        return new LexemeToUnknown();
    }

    protected function getTokenType()
    {
        return TokenType::UNKNOWN;
    }

    public function testShouldNotConvert()
    {
        // everything is converted to unknown
    }

    public function provideInvalidLexemes()
    {
        return array();
    }

    public function provideValidLexemes()
    {
        return array(
            'whatever' => array('!!!')
        );
    }
}

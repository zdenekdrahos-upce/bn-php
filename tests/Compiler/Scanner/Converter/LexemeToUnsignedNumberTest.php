<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\TokenType;

class LexemeToUnsignedNumberTest extends WhenLexemeIsConverted
{
    protected function getConverter()
    {
        return new LexemeToUnsignedNumber();
    }

    protected function getTokenType()
    {
        return TokenType::NUMBER;
    }

    public function provideInvalidLexemes()
    {
        return array(
            'integer' => array(12),
            'float' => array(12.5),
            'plus sign' => array('+12'),
            'minus sign' => array('-12.5')
        );
    }

    public function provideValidLexemes()
    {
        return array(
            'integer' => array('12'),
            'float' => array('12.5'),
        );
    }
}

<?php

namespace BN\Compiler\Scanner\Converter;

abstract class WhenLexemeIsConverted extends \PHPUnit_Framework_TestCase
{
    private $converter;
    private $tokenType;

    protected function setUp()
    {
        $this->converter = $this->getConverter();
        $this->tokenType = $this->getTokenType();
    }

    abstract protected function getConverter();

    abstract protected function getTokenType();

    /** @dataProvider provideInvalidLexemes */
    public function testShouldNotConvert($invalidName)
    {
        $isConvertable = $this->converter->canConvertLexeme($invalidName);
        assertThat($isConvertable, is(false));
    }

    abstract public function provideInvalidLexemes();

    /** @dataProvider provideValidLexemes */
    public function testShouldConvert($name)
    {
        $isConvertable = $this->converter->canConvertLexeme($name);
        assertThat($isConvertable, is(true));

        $token = $this->converter->convertLexeme($name);
        assertThat($token->type, is($this->tokenType));
        assertThat($token->value, is($name));
    }

    abstract public function provideValidLexemes();
}

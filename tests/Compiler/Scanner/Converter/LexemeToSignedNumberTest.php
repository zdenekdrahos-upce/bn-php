<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\TokenType;

class LexemeToSignedNumberTest extends WhenLexemeIsConverted
{
    protected function getConverter()
    {
        return new LexemeToSignedNumber();
    }

    protected function getTokenType()
    {
        return TokenType::NUMBER;
    }

    public function provideInvalidLexemes()
    {
        return array(
            'integer' => array(12),
            'float' => array(12.5),
            'signs +-' => array('+-12'),
            'signs -+' => array('-+12'),
            'signs --' => array('--12'),
            'signs ++' => array('++12'),
        );
    }

    public function provideValidLexemes()
    {
        return array(
            'integer' => array('12'),
            'float' => array('12.5'),
            'plus sign' => array('+12'),
            'minus sign' => array('-12.5')
        );
    }
}

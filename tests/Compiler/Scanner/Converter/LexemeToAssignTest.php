<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\TokenType;

class LexemeToAssignTest extends WhenLexemeIsConverted
{
    private $operator = '=';

    protected function getConverter()
    {
        return new LexemeToAssign($this->operator);
    }

    protected function getTokenType()
    {
        return TokenType::ASSIGN;
    }

    public function provideInvalidLexemes()
    {
        return array(
            'not assign operator' => array('assign')
        );
    }

    public function provideValidLexemes()
    {
        return array(
            'assign' => array($this->operator),
        );
    }
}

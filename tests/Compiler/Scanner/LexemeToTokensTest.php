<?php

namespace BN\Compiler\Scanner;

use BN\Compiler\Token\TokenType;

class LexemeToTokensTest extends \PHPUnit_Framework_TestCase
{
    private $converter;

    protected function setUp()
    {
        $constants = array('numberOfTheBeast');
        $unknown = new Converter\LexemeToUnknown();
        $toToken = new LexemeToToken($unknown);
        $toToken->registerLexer(new Converter\LexemeToUnsignedNumber());
        $toToken->registerLexer(new Converter\LexemeToConstant($constants));
        $this->converter = new LexemeToTokens($toToken);
    }

    public function testShouldReturnNoTokensForEmptyString()
    {
        $this->assertTokens('');
    }

    public function testShouldSeparateLexeme()
    {
        $this->assertTokens('1', TokenType::NUMBER);
        $this->assertTokens('+', TokenType::UNKNOWN);
    }

    public function testShouldSeparateLexemes()
    {
        $this->assertTokens('1+', TokenType::NUMBER, TokenType::UNKNOWN);
    }

    public function testShouldSeparateConstantNameNotValue()
    {
        $this->assertTokens('numberOfTheBeast1', TokenType::CONSTANT, TokenType::NUMBER);
    }

    private function assertTokens()
    {
        $args = func_get_args();
        $lexeme = array_shift($args);
        $expectedTypes = $args;

        $tokens = $this->converter->lexemeToTokens($lexeme);
        assertThat($tokens, arrayWithSize(count($expectedTypes)));
        foreach ($tokens as $t) {
            assertThat($t->type, is(array_shift($expectedTypes)));
        }
    }
}

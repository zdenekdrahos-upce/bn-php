<?php

namespace BN\Compiler;

use Tests\BaseUnitTestCase;
use BN\OperatorsFactory;
use BN\Compiler\Grammar\GrammarBuilder;
use BN\Compiler\Scanner\Converter\LexemeToBNNumber;

class BNNumberEvaluatorTest extends BaseUnitTestCase
{
    private $responder;

    protected function setUp()
    {
        BaseUnitTestCase::setUp();
        $this->responder = $this->mockista->create('BN\Compiler\EvaluatorResponder');
    }

    /** @dataProvider provideMathExpressions */
    public function testShouldCalculateResult($expression, $expectedResult)
    {
        $this->expectsResult($expectedResult);
        $this->evaluateExpression($expression);
    }

    public function provideMathExpressions()
    {
        return array(
            // php float problems
            'http://php.net/manual/en/language.types.float.php'
                => array('floor((0.1+0.7)*10)', '8'),
            'http://stackoverflow.com/questions/211345/working-with-large-numbers-in-php'
                => array('(62574 * 62574) % 104659', '2968'),
            // other tests from version 1.0
            'roundTo #1' => array('26roundTo-5', '25'),
            'roundTo #2' => array('26.26 roundTo 1', '26.3'),
            'negate #1' => array('neg -5 \ 2', '2'),
            'negate #2' => array('neg 5 \ 2', '-2'),
            'power' => array('-2^-5', '-0.03125'),
            'signs, precedence' => array('(-2+5)^(5+5)*5+2-2+-2', '295243')
        );
    }

    private function expectsResult($expectedResult)
    {
        $this->responder->expects('nextStatement')->once;
        $this->responder->expects('result')->once->andCallback(function ($result) use ($expectedResult) {
            assertThat($result instanceof \BN\Number, is(true));
            assertThat($result->__toString(), is($expectedResult));
        });
    }

    public function testShouldCatchExceptions()
    {
        $this->responder->expects('nextStatement')->once;
        $this->responder->expects('exception')->once->andCallback(function($exception) {
            assertThat($exception instanceof \InvalidArgumentException, is(true));
        });
        $this->evaluateExpression('5/0');
    }

    private function evaluateExpression($input)
    {
        $operators = new OperatorsFactory();
        $toNumber = new LexemeToBNNumber();
        $grammar = new GrammarBuilder($toNumber);
        $grammar
            ->statementsSeparator(';')
            ->whiteSpace(',')
            ->assign('=')
            ->brackets('(', ')')
            ->unarySigns('-', '+')
            ->operators($operators->getAll());

        $variables = new \stdClass();
        $interactor = new EvaluatorInteractor($grammar);
        $interactor($input, $variables, $this->responder);
        $this->responder->assertExpectations();
    }
}

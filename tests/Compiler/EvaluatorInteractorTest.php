<?php

namespace BN\Compiler;

use Tests\BaseUnitTestCase;
use Tests\IntegerOperators;
use BN\Compiler\Grammar\GrammarBuilder;
use BN\Compiler\Scanner\Converter\LexemeToUnsignedNumber;

class EvaluatorInteractorTest extends BaseUnitTestCase
{
    private $responder;
    private $variables;

    protected function setUp()
    {
        BaseUnitTestCase::setUp();
        $this->responder = $this->mockista->create('BN\Compiler\EvaluatorResponder');
        $this->variables = new \stdClass();
    }

    /** @dataProvider provideMathExpressions */
    public function testShouldCalculateResult($expression, $expectedResult)
    {
        $this->expectsStatementWithResult($expression, $expectedResult);
        $this->evaluateExpression($expression);
    }

    public function provideMathExpressions()
    {
        return array(
            'basic' => array('1^2+2*3', 7),
            'constant' => array('2+ten', 12),
            'brackets' => array('(1+2)*3', 9),
            'aggregate' => array('sum(1,2,3,2,1) + 2*3', 15),
            'aggregate precedence' => array('sum(1 + 2 3 + 2 1 * 1) + 2*3', 15)
        );
    }

    public function testShouldSupportVariables()
    {
        $expression = '$a = $a + 5';
        $this->variables = (object) array('a' => 5);
        $this->expectsStatementWithResult($expression, 10);
        $this->evaluateExpression($expression);
        assertThat($this->variables->a, is(10));
    }

    /** @dataProvider provideExpectedParserErrors */
    public function testShouldDetectParserError($invalidInput, $expectedErrorMethod)
    {
        $this->expectsOneStatement($invalidInput);
        $this->expectsError($expectedErrorMethod);
        $this->evaluateExpression($invalidInput);
    }

    public function provideExpectedParserErrors()
    {
        return array(
            'unknown token' => array('1 / 2', 'unknownToken'),
            'unknown operator' => array('1 % 2', 'unknownOperator'),
            'mismatched opening bracket' => array('(', 'mismatchedBrackets'),
            'mismatched closing bracket' => array(')', 'mismatchedBrackets')
        );
    }

    /** @dataProvider provideExpectedCalculatorErrors */
    public function testShouldDetectCalculatorError($invalidInput, $expectedErrorMethod)
    {
        $this->expectsStatementWithResult($invalidInput, 0);
        $this->expectsError($expectedErrorMethod);
        $this->evaluateExpression($invalidInput);
    }

    public function provideExpectedCalculatorErrors()
    {
        return array(
            'missing operand' => array('1 +', 'invalidOperands'),
            'missing operator' => array('1 2', 'missingOperator'),
        );
    }

    public function testShouldEvaluateMultipleStatements()
    {
        $twoStatements = '$a=1;$b=2;';
        $this->expectsStatementWithResult('$a=1', '1');
        $this->expectsStatementWithResult('$b=2', '2');
        $this->evaluateExpression($twoStatements);
    }

    private function expectsStatementWithResult($statement, $expectedResult)
    {
        $this->expectsOneStatement($statement);
        $this->responder->expects('result')->once->with($expectedResult);
    }

    private function expectsOneStatement($statement)
    {
        $this->responder->expects('nextStatement')->once->with($statement);
    }

    private function expectsError($expectedErrorMethod)
    {
        $this->responder->expects($expectedErrorMethod)->once;
    }

    private function evaluateExpression($input)
    {
        $integerOperators = new IntegerOperators();
        $toNumber = new LexemeToUnsignedNumber();
        $grammar = new GrammarBuilder($toNumber);
        $grammar
            ->statementsSeparator(';')
            ->whiteSpace(',')
            ->assign('=')
            ->brackets('(', ')')
            ->numberConstant('ten', '10')
            ->operators(
                array(
                    '+' => $integerOperators->plus(),
                    '*' => $integerOperators->multiply(),
                    '^' => $integerOperators->power(),
                    'sum' => $integerOperators->sum()
                )
            )
            ->keyword('%', Token\TokenType::OPERATOR);

        $interactor = new EvaluatorInteractor($grammar);
        $interactor($input, $this->variables, $this->responder);
        $this->responder->assertExpectations();
    }
}

<?php

namespace BN\Compiler\Postfix;

class WhenCalculatorSucceedTest extends GivenPostfixCalculator
{
    /** @dataProvider provideExpressionsAndResult */
    public function testShouldCalculate($expression, $expectedResult)
    {
        $this->whenAdditionOperatorIsRegistered();
        $this->whenSumOperatorIsRegistered();
        $this->assertCalculation($expression, $expectedResult);
    }

    public function provideExpressionsAndResult()
    {
        return array(
            'no args -> zero' => array('', 0),
            'one arg -> arg' => array('1', 1),
            'fixed operands - one operator' => array('1 1 +', 2),
            'fixed operands - two operators' => array('1 2 3 + +', 6),
            'variable operands - 1 arg' => array('1 sum', 1),
            'variable operands - 2 args' => array('1 1 sum', 2),
            'variable operands - 3 args' => array('1 2 3 sum', 6),
            'variable operands - 5 args' => array('1 2 3 4 5 sum', 15),
        );
    }
    /** @dataProvider provideExpressionsWhereResultIsFiveAsWellAsValueInVariableA */
    public function testShouldSupportVariables($expression, $inputVariables = array())
    {
        $this->variables = (object) $inputVariables;
        $this->whenAdditionOperatorIsRegistered();
        $this->assertCalculation($expression, 5);
        assertThat($this->variables->a, is('5'));
    }

    public function provideExpressionsWhereResultIsFiveAsWellAsValueInVariableA()
    {
        return array(
            'use variable in expression' => array('$a 0 +', array('a' => 5)),
            'assign value to variable' => array('$a 5 =')
        );
    }
}

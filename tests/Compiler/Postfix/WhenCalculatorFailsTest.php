<?php

namespace BN\Compiler\Postfix;

use BN\Compiler\Postfix\Operands\OperandsSummary;

class WhenCalculatorFailsTest extends GivenPostfixCalculator
{
    protected function setUp()
    {
        parent::setUp();
        $this->whenAdditionOperatorIsRegistered();
        $this->whenSumOperatorIsRegistered();
    }

    public function testShouldDetectUndefinedType()
    {
        $this->scanner->expectsUnknownToken($this->errorHandler);
        $this->assertResultIsZeroWhenCalculatorFails('UNKNOWN_TOKEN');
    }

    public function testShouldDetectUndefinedOperator()
    {
        $this->scanner->expectsUndefinedOperator($this->errorHandler);
        $this->assertResultIsZeroWhenCalculatorFails('UNKNOWN_OPERATOR');
    }

    public function testShouldDetectMissingOperator()
    {
        $this->errorHandler->expects('missingOperator')->once->with(2);
        $this->assertResultIsZeroWhenCalculatorFails('1 1');
    }

    /** @dataProvider provideExpressionsWithMissingOperands */
    public function testShouldDetectMissingOperand($expression, array $operandsInfo)
    {
        $this->expectsInvalidOperands(
            $operandsInfo['symbol'],
            $operandsInfo['operandsCount'],
            $operandsInfo['expectedCount']
        );
        $this->assertResultIsZeroWhenCalculatorFails($expression);
    }

    public function provideExpressionsWithMissingOperands()
    {
        return array(
            'fixed operands' => array(
                '1 +',
                array('symbol' => '+', 'operandsCount' => 1, 'expectedCount' => 2)
            ),
            'variable operands' => array(
                'sum',
                array('symbol' => 'sum', 'operandsCount' => 0, 'expectedCount' => 1)
            ),
        );
    }

    private function expectsInvalidOperands($symbol, $count, $expectedCount)
    {
        $asserter = function ($symbol, OperandsSummary $o) use ($symbol, $count, $expectedCount) {
            assertThat($symbol, is($symbol));
            assertThat($o->countOperands(), is($count));
            assertThat($o->expectedCount, is($expectedCount));
        };
        $this->errorHandler->expects('invalidOperands')->once->andCallback($asserter);
    }

    /** @dataProvider provideExpressionsWithUndefinedVariable */
    public function testShouldDetectUndefinedVariable($expression, $nameOfUndefined)
    {
        $this->errorHandler->expects('undefinedVariable')->once->with($nameOfUndefined);
        $this->assertResultIsZeroWhenCalculatorFails($expression);
    }

    public function provideExpressionsWithUndefinedVariable()
    {
        return array(
            'undefined variable' => array('$a 5 +', '$a'),
            'undefined constant' => array('CONSTANT 5 +', 'CONSTANT')
        );
    }

    private function assertResultIsZeroWhenCalculatorFails($tokens)
    {
        $this->assertCalculation($tokens, 0);
        $this->errorHandler->assertExpectations();
    }
}

<?php

namespace BN\Compiler\Postfix\Operator;

use BN\Compiler\Postfix\Variables;
use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;

class AssignOperatorTest extends \PHPUnit_Framework_TestCase
{
    private $operator;
    private $variables;

    protected function setUp()
    {
        $this->variables = new \stdClass();
        $vars = new Variables();
        $vars->setVariables($this->variables);
        $this->operator = new AssignOperator($vars);
    }

    public function testShouldAssignValueToVariable()
    {
        $this->whenAssign('$a', 5);
        $this->variableAShouldBeFiveAsWellAsAssigmentResult();
    }

    public function testShouldAssignValuFromVariable()
    {
        $this->variables->existing = 5;
        $this->whenAssign('$a', '$existing');
        $this->variableAShouldBeFiveAsWellAsAssigmentResult();
    }

    public function testShouldFailWhenLeftOperandIsNotVariable()
    {
        $this->whenAssign(2, 5);
        $this->resultShouldBeInvalid();
    }

    private function whenAssign($a, $b)
    {
        $tokenA = $this->createToken($a);
        $tokenB = $this->createToken($b);
        $this->result = $this->operator->__invoke(
            array($tokenA, $tokenB)
        );
    }

    private function createToken($value)
    {
        $type = $value[0] == '$' ? TokenType::VARIABLE : TokenType::NUMBER;
        return new Token($type, $value);
    }

    private function variableAShouldBeFiveAsWellAsAssigmentResult()
    {
        assertThat($this->variables->a, is(5));
        assertThat($this->result, is(5));
    }

    private function resultShouldBeInvalid()
    {
        assertThat($this->result, nullValue());
    }
}

<?php

namespace BN\Compiler\Postfix;

use BN\Collections\Queue;
use Tests\TokenScanner;
use Tests\BaseUnitTestCase;
use BN\Compiler\Grammar\Operators;
use Tests\IntegerOperators;

class GivenPostfixCalculator extends BaseUnitTestCase
{
    private $queue;
    private $evaluator;
    protected $scanner;
    protected $errorHandler;
    protected $operators;
    private $integerOperators;
    protected $variables;

    protected function setUp()
    {
        BaseUnitTestCase::setUp();
        $this->integerOperators = new IntegerOperators();
        $this->errorHandler = $this->mockista->create('BN\Compiler\Postfix\CalculatorErrorHandler');
        $this->queue = new Queue();
        $this->operators = new Operators();
        $this->variables = new \stdClass();
        $constants = array();
        $this->evaluator = new PostfixEvaluator($this->errorHandler, $this->operators, $constants);
        $this->scanner = new TokenScanner();
    }

    public function testShouldReturnNumberWhenNoOperatorIsCalled()
    {
        $this->assertCalculation('', 0);
        $this->assertCalculation('1', 1);
    }

    protected function whenAdditionOperatorIsRegistered()
    {
        $this->operators->register('+', $this->integerOperators->plus());
    }

    protected function whenSumOperatorIsRegistered()
    {
        $this->operators->register('sum', $this->integerOperators->sum());
    }

    protected function assertCalculation($tokens, $expectedResult)
    {
        $this->pushTokens($tokens);
        $this->assertResult($expectedResult);
    }

    private function pushTokens($tokensString)
    {
        foreach ($this->scanner->stringToTokens($tokensString) as $token) {
            $this->queue->push($token);
        }
    }

    private function assertResult($expectedResult)
    {
        $result = $this->evaluator->evaluate($this->queue, $this->variables);
        assertThat($result, is($expectedResult));
    }
}

<?php

namespace BN\Compiler\Parser;

class WhenParserSucceedTest extends GivenShuntingYardParser
{
    protected function setUp()
    {
        parent::setUp();
        $this->registerOperators(array(
            '+' => $this->integerOperators->plus(),
            '*' => $this->integerOperators->multiply(),
            '=' => operator()->precedence(0)->rightAssociative()
        ));
    }

    /** @dataProvider provideExpressionAndExpectedTokens */
    public function testShouldReturnTokenValuesInPostfixNotation($tokensString, array $expectedQueue)
    {
        $tokens = $this->stringToTokens($tokensString);
        $queue = $this->parse($tokens);
        assertThat($queue instanceof \BN\Collections\Queue, is(true));
        while (!empty($expectedQueue)) {
            $expectedValue = array_shift($expectedQueue);
            assertThat($queue->pop()->value, is($expectedValue));
        }
    }

    public function provideExpressionAndExpectedTokens()
    {
        return array(
            'no tokens -> empty queue' => array('', array()),
            'only brackets -> empty queue' => array('( )', array()),
            'one token -> one token in queue' => array('2', array(2)),
            'default precedence is *, +' => array('3 + 4 * 2', array(3, 4, 2, '*', '+')),
            'brackets should change precedence' => array('( 3 + 4 ) * 2', array(3, 4, '+', 2, '*')),
            'variables are treated as numbers' => array('$a + $b', array('$a', '$b', '+')),
            'assign is treated as operator' => array('$a = 5', array('$a', '5', '='))
        );
    }
}

<?php

namespace BN\Compiler\Parser;

use Tests\BaseUnitTestCase;
use Tests\TokenScanner;
use BN\Compiler\Grammar\Operators;
use Tests\IntegerOperators;

class GivenShuntingYardParser extends BaseUnitTestCase
{
    private $parser;
    protected $errorHandler;
    protected $scanner;
    protected $integerOperators;
    private $operators;

    protected function setUp()
    {
        BaseUnitTestCase::setUp();
        $this->integerOperators = new IntegerOperators();
        $this->errorHandler = $this->mockista->create('BN\Compiler\Parser\ParserErrorHandler');
        $this->operators = new Operators();
        $this->parser = new ShuntingYardParser($this->errorHandler, $this->operators);
        $this->scanner = new TokenScanner();
    }

    protected function registerOperators(array $operators)
    {
        foreach ($operators as $symbol => $o) {
            $this->operators->register($symbol, $o);
        }
    }

    protected function parse(array $tokens)
    {
        return $this->parser->parse($tokens);
    }

    protected function stringToTokens($tokensString)
    {
        return $this->scanner->stringToTokens($tokensString);
    }
}

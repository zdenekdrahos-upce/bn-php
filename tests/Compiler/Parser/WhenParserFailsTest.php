<?php

namespace BN\Compiler\Parser;

class WhenParserFailsTest extends GivenShuntingYardParser
{

    public function testShouldDetectMismatchedBrackets()
    {
        $this->assertMismatchedBracket('(');
        $this->assertMismatchedBracket(')');
    }

    private function assertMismatchedBracket($bracket)
    {
        $this->errorHandler->expects('mismatchedBrackets')->once->with($bracket);
        $this->parserShouldFail($bracket);
    }

    public function testShouldDetectUndefinedOperator()
    {
        $this->scanner->expectsUndefinedOperator($this->errorHandler);
        $this->parserShouldFail('UNKNOWN_OPERATOR');
    }

    public function testShouldDetectUnknownToken()
    {
        $this->scanner->expectsUnknownToken($this->errorHandler);
        $this->parserShouldFail('UNKNOWN_TOKEN');
    }

    private function parserShouldFail($tokenString)
    {
        $tokens = $this->stringToTokens($tokenString);
        $result = $this->parse($tokens);
        assertThat($result, is(false));
        $this->mockista->assertExpectations();
    }
}

<?php
/**
 * BN-PHP (https://bitbucket.org/zdenekdrahos/bn-php)
 * @license New BSD License
 * @author Zdenek Drahos
 */

/**
 * Bootstrap - PHPUnit
 */

/**
 * Set default scale parameter for all BCMath functions
 */
bcscale(20);

// Composer autoload
require_once(__DIR__ . '/vendor/autoload.php');
require_once(__DIR__ . '/helpers/functions.php');

require_once 'Hamcrest/hamcrest.php';

spl_autoload_register('testsAutoload');

require_once(__DIR__ . '/Compiler/Parser/GivenShuntingYardParser.php');
require_once(__DIR__ . '/Compiler/Postfix/GivenPostfixCalculator.php');
require_once(__DIR__ . '/Compiler/Scanner/Converter/WhenLexemeIsConverted.php');

// BN-PHP autoload
require_once(__DIR__ . '/../demo/autoload.php');

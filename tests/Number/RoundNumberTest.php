<?php

namespace BN;

class RoundNumberTest extends \PHPUnit_Framework_TestCase
{
    /** @dataProvider provideValidRoundData */
    public function testRound($a, $precision, $expectedNumber)
    {
        $a = new Number($a);
        $this->assertNumbers($expectedNumber, $a->round($precision));
    }

    public function provideValidRoundData()
    {
        return array(
            array('123456789.987654321', 5, '123456789.98765'),
            array('123456789.987654321', 2, '123456789.99'),
            array('123456789.987654321', 0, '123456790'),
            array('123456789.987654321', -2, '123456800'),
            array('123456789.987654321', -5, '123500000'),
            array('12', 5, '12'),
            array('12', 0, '12'),
            array('0.5', -5, '0'),
            array('0.5', 0, '1'),
            array('0.000055', 3, '0.000'),
            array('0.000055', 4, '0.0001'),
            array('0.000055', 5, '0.00006'),
            array('-12499', -2, '-12500'),
            array('-12401', -2, '-12400'),
            array('12.000000000000000000000', 0, '12'),
            array('2.5', new Number('0'), '3'),
        );
    }

    /** @dataProvider provideRoundUpData */
    public function testRoundUp($a, $expectedNumber)
    {
        $a = new Number($a);
        $this->assertNumbers($expectedNumber, $a->roundUp());
    }

    public function provideRoundUpData()
    {
        return array(
            array('9.0000000000000000000001', '10'),
            array('10.0000000000000000000000', '10'),
            array('-9.256', '-10'),
            array('-10', '-10'),
        );
    }

    /** @dataProvider provideRoundDownData */
    public function testRoundDown($a, $expectedNumber)
    {
        $a = new Number($a);
        $this->assertNumbers($expectedNumber, $a->roundDown());
    }

    public function provideRoundDownData()
    {
        return array(
            array('2.00', '2'),
            array('1.2', '1'),
            array('-1.2', '-1'),
            array('-2', '-2'),
        );
    }

    /** @dataProvider provideValidRoundToNumberData */
    public function testRoundToNumber($a, $precision, $expectedNumber)
    {
        $a = new Number($a);
        $this->assertNumbers($expectedNumber, $a->roundToNumber($precision));
    }

    public function provideValidRoundToNumberData()
    {
        return array(
            array('124.655', -100, '100'),
            array('124.655', -15, '120'),
            array('124.655', -10, '120'),
            array('124.655', 0, '125'),
            array('124.655', 4, '124.8'),
            array('124.655', 10, '124.70'),
            array('124.655', 50, '124.50'),
            array('124.655', 100, '124.700'),
            array('21.60', 50, '21.50'),
            array('21.60', 70, '21.70'),
            array('21.60', 40, '21.80'),
            array('21.60', 7, '21.7'),
            array('21.60', -30, '30'),
            array('21.60', -50, '0'),
            array('12', 5, '12'),
            array('2.4', -5, '0'),
            array('2.5', -5, '5'),
            array('3.5', -5, '5'),
            array('0.12345', 10, '0.10'),
            array('0.12345', 20, '0.20'),
            array('0.12345', 40, '0'),
            array('22', new Number('-10'), '20'),
        );
    }

    /** @dataProvider provideInvalidPrecision */
    public function testRoundShouldThrowExceptionWhenPrecisionIsNotInt($precision)
    {
        parent::setExpectedException('InvalidArgumentException');
        $number = new Number('-10');
        $number->round($precision);
    }

    /** @dataProvider provideInvalidPrecision */
    public function testRoundToShouldThrowExceptionWhenPrecisionIsNotInt($precision)
    {
        parent::setExpectedException('InvalidArgumentException');
        $number = new Number('-10');
        $number->roundToNumber($precision);
    }

    public function provideInvalidPrecision()
    {
        return array(
            'string' => array('five'),
            'float' => array(2.5)
        );
    }

    private function assertNumbers($a, INumber $b)
    {
        $a = new Number($a);
        parent::assertSame(0, bccomp($a->__toString(), $b->__toString()));
    }
}

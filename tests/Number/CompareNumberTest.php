<?php

namespace BN;

class CompareNumberTest extends \PHPUnit_Framework_TestCase
{
    /** @dataProvider provideIsIntegerData */
    public function testIsInteger($a, $isInteger)
    {
        $a = new Number($a);
        assertThat($a->isInteger(), is($isInteger));
    }

    public function provideIsIntegerData()
    {
        return array(
            array('11', true),
            array('12.000000', true),
            array('51.', true),
            array('-5.000000', true),
            array('11.51', false),
            array('12.0000001', false),
            array('-81.000001', false)
        );
    }

    /** @dataProvider provideIsZeroData */
    public function testIsZero($a, $isZero)
    {
        $a = new Number($a);
        assertThat($a->isZero(), is($isZero));
    }

    public function provideIsZeroData()
    {
        return array(
            array('0', true),
            array('1', false),
            array('0.0000005', false),
            array('-5', false)
        );
    }

    /** @dataProvider provideIsNegativeData */
    public function testIsNegative($a, $isNegative)
    {
        $a = new Number($a);
        assertThat($a->isNegative(), is($isNegative));
    }

    public function provideIsNegativeData()
    {
        return array(
            array('-0.00005', true),
            array('-5555', true),
            array('0', false),
            array('56556', false)
        );
    }

    /** @dataProvider provideIsPositiveData */
    public function testIsPositive($a, $isNegative)
    {
        $a = new Number($a);
        assertThat($a->isPositive(), is($isNegative));
    }

    public function provideIsPositiveData()
    {
        return array(
            array('-0.00005', false),
            array('-5555', false),
            array('0', false),
            array('11', true),
            array('56556', true)
        );
    }

    public function testIsEqual()
    {
        $number = new Number('20');
        assertThat($number->isEqual($number), is(true));
        assertThat($number->compare($number), is(0));
    }

    public function testCompareNumbers()
    {
        $smallerNumber = new Number('-50');
        $largerNumber = new Number('10');

        assertThat($largerNumber->isBiggerThan($smallerNumber), is(true));
        assertThat($largerNumber->compare($smallerNumber), is(1));

        assertThat($smallerNumber->isSmallerThan($largerNumber), is(true));
        assertThat($smallerNumber->compare($largerNumber), is(-1));
    }
}

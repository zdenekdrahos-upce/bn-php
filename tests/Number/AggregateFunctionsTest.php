<?php

namespace BN;

use BN\Number;

class AggregateFunctionsTest extends \PHPUnit_Framework_TestCase
{
    private $aggregate;

    protected function setUp()
    {
        $this->aggregate = new AggregateFunctions();
    }

    /** @dataProvider provideFunctionsWhichShouldReturnZeroForNoOperands */
    public function testShouldReturnZeroForNoOperands($function)
    {
        $this->assertResult($function, array(), 0);
    }

    public function provideFunctionsWhichShouldReturnZeroForNoOperands()
    {
        return array(
            'sum' => array('sum'),
            'count' => array('count'),
            'avg' => array('avg'),
        );
    }

    /** @dataProvider provideFunctionsWhichShouldReturnNullForNoOperands */
    public function testShouldReturnNullForNoOperands($function)
    {
        $result = $this->aggregate->$function(array());
        assertThat($result, nullValue());
    }

    public function provideFunctionsWhichShouldReturnNullForNoOperands()
    {
        return array(
            'min' => array('min'),
            'max' => array('max'),
        );
    }

    /** @dataProvider whenOperandIsThree */
    public function testShouldWorkForAtLeastOneOperands($function, $expectedResult)
    {
        $operands = array(new Number('3'));
        $this->assertResult($function, $operands, $expectedResult);
    }

    public function whenOperandIsThree()
    {
        return array(
            'sum' => array('sum', 3),
            'count' => array('count', 1),
            'avg' => array('avg', 3),
            'min' => array('min', 3),
            'max' => array('max', 3),
        );
    }

    /** @dataProvider whenOperandIsOneTwoAndThree */
    public function testShouldAggregateNumbersIntoOneNumber($function, $expectedResult)
    {
        $operands = array(new Number('1'), new Number('2'), new Number('3'));
        $this->assertResult($function, $operands, $expectedResult);
    }

    public function whenOperandIsOneTwoAndThree()
    {
        return array(
            'sum' => array('sum', 6),
            'count' => array('count', 3),
            'avg' => array('avg', 2),
            'min' => array('min', 1),
            'max' => array('max', 3),
        );
    }

    private function assertResult($function, array $operands, $expectedResult)
    {
        $result = $this->aggregate->$function($operands);
        assertThat($result instanceof Number, is(true));
        assertThat((string)$result, is((string)$expectedResult));
    }
}

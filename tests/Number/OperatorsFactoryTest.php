<?php

namespace BN;

class OperatorsFactoryTest extends \PHPUnit_Framework_TestCase
{
    private $factory;

    protected function setUp()
    {
        $this->factory = new OperatorsFactory();
    }

    private function assertResult($operator, array $operands, $expectedResult)
    {
        $operator = $this->factory->create($operator)->buildEvaluator();
        $result = $operator($operands);
        assertThat($result instanceof Number, is(true));
        assertThat((string) $result, is((string) $expectedResult));
    }

    /** @dataProvider whenOperandIsThreeAndTwo */
    public function testShouldCreateBinaryOperator($operator, $expectedResult)
    {
        $operands = array(new Number('3'), new Number('2'));
        $this->assertResult($operator, $operands, $expectedResult);
    }

    public function whenOperandIsThreeAndTwo()
    {
        return array(
            'add' => array('+', 5),
            'subtract' => array('-', 1),
            'multiply' => array('*', 6),
            'divide' => array('/', 1.5),
            'quotient' => array('\\', 1),
            'modulo' => array('%', 1),
            'power' => array('^', 9),
        );
    }

    /** @dataProvider whenOperandIsTwoPointTwentySeven */
    public function testShouldCreateBinaryOperatorWithIntegerPrecision($operator, $precision, $expectedResult)
    {
        $operands = array(new Number('2.25'), new Number((string) $precision));
        $this->assertResult($operator, $operands, $expectedResult);
    }

    public function whenOperandIsTwoPointTwentySeven()
    {
        return array(
            'round' => array('round', 1, 2.3),
            'roundTo' => array('roundTo', 20, 2.2)
        );
    }

    /** @dataProvider whenOperandIsTwoPointTwentyFive */
    public function testShouldCreateUnaryOperator($operator, $expectedResult)
    {
        $operands = array(new Number('2.25'));
        $this->assertResult($operator, $operands, $expectedResult);
    }

    public function whenOperandIsTwoPointTwentyFive()
    {
        return array(
            'sqrt' => array('sqrt', 1.5),
            'abs' => array('abs', 2.25),
            'neg' => array('neg', -2.25),
            'floor' => array('floor', 2),
            'ceil' => array('ceil', 3),
        );
    }

    /** @dataProvider whenOperandIsOneTwoAndThree */
    public function testShouldCreateAggregationOperator($operator, $expectedResult)
    {
        $operands = array(new Number('1'), new Number('2'), new Number('3'));
        $this->assertResult($operator, $operands, $expectedResult);
    }

    public function whenOperandIsOneTwoAndThree()
    {
        return array(
            'sum' => array('sum', 6),
            'count' => array('count', 3),
            'avg' => array('avg', 2),
            'min' => array('min', 1),
            'max' => array('max', 3),
        );
    }
}

<?php

namespace BN;

class NumberTest extends \PHPUnit_Framework_TestCase
{
    /** @var Number */
    private $numberZero;
    /** @var Number */
    private $numberTen;
    /** @var Number */
    private $numberNegativeTen;

    protected function setUp()
    {
        parent::setUp();
        $this->numberZero = new Number();
        $this->numberTen = new Number('10');
        $this->numberNegativeTen = new Number('-10');
    }

    /** @dataProvider provideNonNumericStrings */
    public function testConstructorShouldThrowException($input)
    {
        parent::setExpectedException('InvalidArgumentException');
        new Number($input);
    }

    public function provideNonNumericStrings()
    {
        return array(
            'int' => array(111),
            'string' => array('Hello'),
            'null' => array(null),
        );
    }

    public function testShouldAddAndSubtract()
    {
        $number = new Number('1000000');
        $this->assertNumbers('1000000', $this->numberZero->add($number));
        $this->assertNumbers('-1000000', $this->numberZero->subtract($number));
    }

    public function testShouldMultiplyAndDivide()
    {
        $number = new Number('-1000000');
        $this->assertNumbers('-10000000', $this->numberTen->multiply($number));
        $this->assertNumbers('-0.00001', $this->numberTen->divide($number));
    }

    /** @dataProvider provideMethodsWhichThrowExceptionWhenArgumentIsZero */
    public function testDivideByZeroShouldThrowException($method)
    {
        parent::setExpectedException('InvalidArgumentException');
        $this->numberTen->$method($this->numberZero);
    }

    public function provideMethodsWhichThrowExceptionWhenArgumentIsZero()
    {
        return array(array('divide'), array('quotient'), array('modulo'));
    }

    /** @dataProvider provideQuotientData */
    public function testShouldCalculateQuotient($a, $b, $expectedQuotient)
    {
        $a = new Number($a);
        $b = new Number($b);
        $this->assertNumbers($expectedQuotient, $a->quotient($b));
    }

    public function provideQuotientData()
    {
        return array(
            array('10', '3', '3'),
            array('10', '-3', '-3'),
            array('-10', '3', '-3'),
            array('-10', '-3', '3'),
        );
    }

    /** @dataProvider provideModuloData */
    public function testShouldCalculateModulo($a, $b, $expectedModulo)
    {
        $a = new Number($a);
        $b = new Number($b);
        $this->assertNumbers($expectedModulo, $a->modulo($b));
    }

    public function provideModuloData()
    {
        return array(
            array('0', '10', '0'),
            array('10', '4', '2'),
            array('10', '2.1', '1.6'),
            array('10', '-2.1', '1.6'),
            array('-10', '2.1', '-1.6'),
            array('-10', '-2.1', '-1.6'),
        );
    }

    /** @dataProvider providePowerData */
    public function testShouldCalculatePower($a, $b, $expectedPower)
    {
        $a = new Number($a);
        $b = new Number($b);
        $this->assertNumbers($expectedPower, $a->power($b));
    }

    public function providePowerData()
    {
        return array(
            array('0', '10', '0'),
            array('10', '0', '1'),
            array('10', '6', '1000000'),
            array('10', '-10', '0.0000000001'),
            'sqrt is used if power = 1/2' => array('4', '0.5', '2'),
            'float is used if power is decimal and its not sqrt' => array('16', '0.25', '2'),
        );
    }

    /** @dataProvider provideSqrtData */
    public function testShouldCalculateSqrt($a, $expectedSqrt)
    {
        $a = new Number($a);
        $this->assertNumbers($expectedSqrt, $a->sqrt());
    }

    public function provideSqrtData()
    {
        return array(
            array('0', '0'),
            array('99999980000001', '9999999'),
        );
    }

    public function testNegativeNumberSqrtShouldThrowException()
    {
        parent::setExpectedException('InvalidArgumentException');
        $this->numberNegativeTen->sqrt();
    }

    /** @dataProvider provideAbsData */
    public function testShouldCalculateAbsoluteValue($a, $expectedAbs)
    {
        $a = new Number($a);
        $this->assertNumbers($expectedAbs, $a->abs());
    }

    public function provideAbsData()
    {
        return array(
            array('0', '0'),
            array('10', '10'),
            array('-10', '10')
        );
    }

    /** @dataProvider provideNegData */
    public function testShouldCalculateNegate($a, $expectedNeg)
    {
        $a = new Number($a);
        $this->assertNumbers($expectedNeg, $a->negate());
    }

    public function provideNegData()
    {
        return array(
            array('-11', '11'),
            array('0', '0'),
            array('11', '-11')
        );
    }

    public function testScale()
    {
        // precondition: global is at least 3
        $a = new Number('1.234');
        $b = new Number('5');
        $a->setLocalScale(0);
        $this->assertNumbers('6', $a->add($b));
        $this->assertNumbers('6.234', $b->add($a)); // scale is defined only in $a
        // reset -> global scale will be used
        $a->resetLocalScale();
        $this->assertNumbers('6.234', $a->add($b));
    }

    public function testToString()
    {
        parent::assertSame('0', $this->numberZero->__toString());
    }

    private function assertNumbers($a, INumber $b)
    {
        $a = new Number($a);
        parent::assertSame(0, bccomp($a->__toString(), $b->__toString()));
    }
}

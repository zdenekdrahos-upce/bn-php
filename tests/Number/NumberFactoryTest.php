<?php

namespace BN;

class NumberFactoryTest extends \PHPUnit_Framework_TestCase
{
    /** @var NumberFactory */
    private $factory;

    protected function setUp()
    {
        parent::setUp();
        $this->factory = new NumberFactory();
    }

    /** @dataProvider provideValidInputs */
    public function testShouldCreateNumber($input)
    {
        $createdNumber = $this->factory->createNumber($input);
        parent::assertInstanceOf('\BN\INumber', $createdNumber);
        parent::assertSame(0, bccomp((string) $input, (string) $createdNumber));
    }

    public function provideValidInputs()
    {
        return array(
            'from string' => array('569'),
            'from integer' => array(569),
            'from float #1' => array(569.52),
            'from float #2' => array(1.2e3),
            'from float #3' => array(7E-10),
            'from object' => array(new \Tests\ZeroNumberObject())
        );
    }

    /** @dataProvider provideInvalidInputs */
    public function testShouldThrowException($invalidInput)
    {
        parent::setExpectedException('InvalidArgumentException');
        $this->factory->createNumber($invalidInput);
    }

    public function provideInvalidInputs()
    {
        return array(
            'object without __toString' => array(new \stdClass()),
            'array is not allowed' => array(array(5, 6, '558'))
        );
    }

    /** @dataProvider provideConstants */
    public function testShouldCreateConstants($constantMethod, $approximation)
    {
        $number = $this->factory->$constantMethod();
        parent::assertInstanceOf('\BN\INumber', $number);
        // 200 digits after decimal point + decimal point + first nummber = 202
        parent::assertSame(202, strlen((string)$number));
        parent::assertSame($approximation, (string)$number->round(3));
    }

    public function provideConstants()
    {
        return array(
            'pi' => array('createPI', '3.142'),
            'e' => array('createEulerNumber', '2.718')
        );
    }
}

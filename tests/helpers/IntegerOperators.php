<?php

namespace Tests;

class IntegerOperators
{
    public function plus()
    {
        return operator()
            ->precedence(2)->leftAssociative()
            ->binary(function ($a, $b) {
                return $a + $b;
            });
    }

    public function multiply()
    {
        return operator()
            ->precedence(3)->leftAssociative()
            ->binary(function ($a, $b) {
                return $a * $b;
            });
    }

    public function power()
    {
        return operator()
            ->precedence(4)->rightAssociative()
            ->binary(function ($a, $b) {
                return pow($a, $b);
            });
    }

    public function sum()
    {
        return operator()
            ->precedence(1)->leftAssociative()
            ->aggregate(function (array $operands) {
                $sum = 0;
                foreach ($operands as $o) {
                    $sum += $o;
                }
                return $sum;
            });
    }
}

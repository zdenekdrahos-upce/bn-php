<?php

function operator()
{
    return new BN\Compiler\Grammar\OperatorBuilder();
}

function testsAutoload($class)
{
    if (strpos($class, 'Tests\\') === 0) {
        $classPath = str_replace(
            array('\\', 'Tests'),
            array(DIRECTORY_SEPARATOR, ''),
            $class
        );
        require_once(__DIR__ . "/{$classPath}.php");
    }
}

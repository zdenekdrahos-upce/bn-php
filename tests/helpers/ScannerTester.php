<?php

namespace Tests;

use BN\Compiler\Token\Token;
use BN\Compiler\Scanner\Statement;
use BN\Compiler\Scanner\LexemeToToken;
use BN\Compiler\Scanner\LexemeToTokens;
use BN\Compiler\Scanner\Converter\LexemeToUnknown;
use BN\Compiler\Scanner\Converter\LexemeToKeyword;

class ScannerTester
{
    private $statementsSeparator = ';';
    private $whiteSpace = array("\n");
    private $keywords;

    private $lexemeToNumber;
    private $scannerDecorator;
    private $statementsComparer;

    public function __construct(array $setup)
    {
        $this->keywords = $setup['keywords'];
        $this->lexemeToNumber = $setup['toNumber'];
        $this->scannerDecorator = $setup['scanner'];
        $this->statementsComparer = $setup['statementsComparer'];
    }

    public function assertStatementsCount($input, $expectedStatementsCount)
    {
        $scanner = $this->buildScanner();
        $statements = $scanner->tokenize($input);
        assertThat($statements, arrayWithSize($expectedStatementsCount));
    }

    public function shouldFindOneToken($input, $expectedValue, $expectedType)
    {
        $this->shouldFindOneStatement($input, array($expectedValue => $expectedType));
    }

    public function shouldFindOneStatement($input, array $expectedTokens)
    {
        $scanner = $this->buildScanner();
        $statements = $scanner->tokenize($input);

        $this->assertOneStatement($statements, $input);

        $tokens = $statements[0]->tokens;
        assertThat($tokens, arrayWithSize(count($expectedTokens)));

        foreach ($expectedTokens as $value => $type) {
            $token = array_shift($tokens);
            $this->assertToken($token, $value, $type);
        }
    }

    private function assertOneStatement($statements, $input)
    {
        assertThat($statements, arrayWithSize(1));
        $statement = $statements[0];
        assertThat($statement instanceof Statement, is(true));
        $this->statementsComparer->__invoke($input, $statement->statement);
    }

    private function buildScanner()
    {
        $unknown = new LexemeToUnknown();
        $toToken = new LexemeToToken($unknown);
        $toToken->registerLexer($this->lexemeToNumber);
        $toToken->registerLexer(new LexemeToKeyword($this->keywords));
        $converter = new LexemeToTokens($toToken);
        $scanner = new \BN\Compiler\Scanner\Scanner($converter, $this->whiteSpace, $this->statementsSeparator);
        return $this->scannerDecorator->__invoke($scanner);
    }

    private function assertToken(Token $token, $expectedValue, $expectedType)
    {
        assertThat($token->value, is($expectedValue));
        assertThat($token->type, is($expectedType));
    }
}

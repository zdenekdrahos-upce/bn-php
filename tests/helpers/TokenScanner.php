<?php

namespace Tests;

use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;

class TokenScanner
{
    protected $unknownToken;
    protected $unknownOperator;

    public function __construct()
    {
        $this->unknownToken = new Token(TokenType::UNKNOWN, 'UNKNOWN_TOKEN');
        $this->unknownOperator = new Token(TokenType::OPERATOR, 'UNKNOWN_OPERATOR');
    }

    public function expectsUndefinedOperator($errorHandler)
    {
        $errorHandler->expects('unknownOperator')->once->with($this->unknownOperator->value);
    }

    public function expectsUnknownToken($errorHandler)
    {
        $errorHandler->expects('unknownToken')->once->with($this->unknownToken->value);
    }

    public function stringToTokens($tokensString)
    {
        $tokens = array();
        foreach ($this->explodeTokens($tokensString) as $arg) {
            $token = $arg;
            if (is_numeric($arg)) {
                $token = new Token(TokenType::NUMBER, (int) $arg);
            } elseif ($arg == $this->unknownToken->value) {
                $token = $this->unknownToken;
            } elseif ($arg == $this->unknownOperator->value) {
                $token = $this->unknownOperator;
            } elseif ($arg == '(') {
                $token = new Token(TokenType::BRACKET_OPENING, $arg);
            } elseif ($arg == ')') {
                $token = new Token(TokenType::BRACKET_CLOSING, $arg);
            } elseif ($arg[0] == '$') {
                $token = new Token(TokenType::VARIABLE, $arg);
            } elseif ($arg == 'CONSTANT') {
                $token = new Token(TokenType::CONSTANT, $arg);
            } elseif ($arg[0] == '=') {
                $token = new Token(TokenType::ASSIGN, $arg);
            } elseif (is_string($arg)) {
                $token = new Token(TokenType::OPERATOR, $arg);
            }
            $tokens[] = $token;
        }
        return $tokens;
    }

    private function explodeTokens($tokensString)
    {
        return array_filter(explode(' ', $tokensString), 'strlen');
    }
}

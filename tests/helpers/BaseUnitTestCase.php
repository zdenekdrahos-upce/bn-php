<?php

namespace Tests;

abstract class BaseUnitTestCase extends \PHPUnit_Framework_TestCase
{

    /** @var \Mockista\Registry */
    protected $mockista;

    protected function setUp()
    {
        $this->mockista = new \Mockista\Registry();
    }

    protected function tearDown()
    {
        if ($this->mockista) {
            $this->mockista->assertExpectations();
        }
    }
}

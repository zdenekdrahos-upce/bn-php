<?php
/**
 * BN-PHP (https://bitbucket.org/zdenekdrahos/bn-php)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace BN\Collections;

/**
 * Exception thrown if null is inserted into a collection.
 */
class NullArgumentException extends \InvalidArgumentException
{
}

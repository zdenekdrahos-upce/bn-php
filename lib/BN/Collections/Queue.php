<?php
/**
 * BN-PHP (https://bitbucket.org/zdenekdrahos/bn-php)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace BN\Collections;

/**
 * Class represeting FIFO data structure. The first element added
 * to the queue will be the first one to be removed.
 */
class Queue extends LinearDataStructure
{
    /**
     * Inserts element at the end of the queue
     * @param  mixed                                 $element
     * @throws \BN\Collections\NullArgumentException
     */
    public function push($element)
    {
        parent::checkIfElementIsNotNull($element);
        array_push($this->data, $element);
    }

    /**
     * Removes and returns first element of the queue.
     * Returns null if the queue is empty
     * @return mixed
     */
    public function pop()
    {
        if (!$this->isEmpty()) {
            return array_shift($this->data);
        }
        return null;
    }

    /**
     * Returns first element of the queue (element is not removed from the queue).
     * Returns null if the queue is empty
     * @return mixed
     */
    public function peek()
    {
        if (!$this->isEmpty()) {
            return $this->data[0];
        }
        return null;
    }

    /**
     * Clones current queue
     * @return \BN\Collections\Queue
     */
    public function __clone()
    {
        $queue = new Queue();
        $queue->data = $this->data;
        return $queue;
    }
}

<?php
/**
 * BN-PHP (https://bitbucket.org/zdenekdrahos/bn-php)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace BN\Collections;

/**
 * Class represeting LIFO data structure. The last element added
 * to the stack will be the first one to be removed.
 */
class Stack extends LinearDataStructure
{
    /**
     * Inserts element to the beggining of the stack
     * @param  mixed                                 $element
     * @throws \BN\Collections\NullArgumentException
     */
    public function push($element)
    {
        parent::checkIfElementIsNotNull($element);
        array_unshift($this->data, $element);
    }

    /**
     * Removes and returns first element of the stack.
     * Returns null if the stack is empty
     * @return mixed
     */
    public function pop()
    {
        if (!$this->isEmpty()) {
            return array_shift($this->data);
        }
        return null;
    }

    /**
     * Returns first element of the stack (element is not removed from the stack).
     * Returns null if the stack is empty
     * @return mixed
     */
    public function peek()
    {
        if (!$this->isEmpty()) {
            return $this->data[0];
        }
        return null;
    }
}

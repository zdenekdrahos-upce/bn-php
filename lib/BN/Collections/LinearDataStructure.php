<?php
/**
 * BN-PHP (https://bitbucket.org/zdenekdrahos/bn-php)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace BN\Collections;

/**
 * Abstract parent class of all linear data structures (queue, stack or
 * object array). It defines basic methods clear, isEmpty, size and protected
 * array for data stored in data structure.
 */
abstract class LinearDataStructure
{
    /** @var array */
    protected $data = array();

    /**
     * Removes all elements from structure. After clear size is equal to 0
     */
    public function clear()
    {
        $this->data = array();
    }

    /** @return boolean */
    public function isEmpty()
    {
        return $this->size() == 0;
    }

    /** @return int */
    public function size()
    {
        return count($this->data);
    }

    protected function checkIfElementIsNotNull($element)
    {
        if (is_null($element)) {
            throw new NullArgumentException('Null inserted into the queue');
        }
    }
}

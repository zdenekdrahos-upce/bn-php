<?php

namespace BN;

class AggregateFunctions
{
    private $zero;

    public function __construct()
    {
        $this->zero = new Number('0');
    }

    public function sum(array $operands)
    {
        if (count($operands) == 0) {
            return $this->zero;
        }
        $sum = array_shift($operands);
        foreach ($operands as $o) {
            $sum = $sum->add($o);
        }
        return $sum;
    }

    public function count(array $operands)
    {
        $count = count($operands);
        return new Number((string) $count);
    }

    public function avg(array $operands)
    {
        $sum = $this->sum($operands);
        $count = $this->count($operands);
        return $count->isZero() ? $this->zero : $sum->divide($count);
    }

    public function min(array $operands)
    {
        return $this->findExtreme($operands, 'isSmallerThan');
    }

    public function max(array $operands)
    {
        return $this->findExtreme($operands, 'isBiggerThan');
    }

    private function findExtreme(array $operands, $compareMethod)
    {
        $extreme = array_shift($operands);
        foreach ($operands as $o) {
            if ($o->$compareMethod($extreme)) {
                $extreme = $o;
            }
        }
        return $extreme;
    }
}

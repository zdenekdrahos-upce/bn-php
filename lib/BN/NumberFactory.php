<?php
/**
 * BN-PHP (https://bitbucket.org/zdenekdrahos/bn-php)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace BN;

/**
 * Factory for creating @see Number from string, integer, float or object.
 * Useful for creating numbers from different types because Number
 * throws \InvalidArgumentException if argument in constructor is not string.
 * It can also create numbers for mathematical constants pi and e.
 */
class NumberFactory
{
    /** @var array */
    private $constants;

    public function __construct()
    {
        $this->loadConstants();
    }

    /**
     * Try to create new @see Number from an argument.
     * @param  string|int|float|object   $number
     * @return \BN\INumber
     * @throws \InvalidArgumentException if $number is not numeric string, int, float
     *  or object with __toString() method
     */
    public function createNumber($number)
    {
        if ($this->isValidNumber($number)) {
            return new Number((string) $number);
        } else {
            throw new \InvalidArgumentException('Argument must be numeric string, int, float, object with __toString');
        }
    }

    /**
     * Returns a new number which represents a mathematical constant π (PI).
     * Precision of the constant is 200 digits after decimal point.
     * @return \BN\Number
     */
    public function createPI()
    {
        return clone $this->constants['pi'];
    }

    /**
     * Returns a new number which represents a mathematical constant e (Euler's
     * number). Precision of the constant is 200 digits after decimal point.
     * @return \BN\Number
     */
    public function createEulerNumber()
    {
        return clone $this->constants['e'];
    }

    private function isValidNumber($number)
    {
        return is_string($number) || is_int($number) || is_float($number)
                || $this->canBeObjectConvertedToString($number);
    }

    private function canBeObjectConvertedToString($object)
    {
        return is_object($object) && method_exists($object, '__toString');
    }

    private function loadConstants()
    {
        $this->constants = array(
            'pi' => new Number($this->getPI()),
            'e' => new Number($this->getE()),
        );
    }

    private function getPI()
    {
        $pi = '3.141592653589793238462643383279502884197169399375105820974944592307816406286208998628034825342117067';
        $pi .= '98214808651328230664709384460955058223172535940812848111745028410270193852110555964462294895493038196';
        return $pi;
    }

    private function getE()
    {
        $e = '2.7182818284590452353602874713526624977572470936999595749669676277240766303535475945713821785251664274';
        $e .= '2746639193200305992181741359662904357290033429526059563073813232862794349076323382988075319525101901';
        return $e;
    }
}

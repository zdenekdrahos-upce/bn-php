<?php
/**
 * BN-PHP (https://bitbucket.org/zdenekdrahos/bn-php)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace BN;

/**
 * Big Number data type for PHP. Uses BC Math for arbitrary precision mathematics
 * with numbers of any size and precision. Object is immutable and the inner
 * representation of number is a string.
 */
class Number implements INumber
{
    /** @var string */
    private $number;
    /** @var int */
    private $scale;

    /**
     * Sets a number and reset local scale (instance will use only global scale).
     * @param  string                    $number
     * @throws \InvalidArgumentException if $number is not numeric string
     */
    public function __construct($number = '0')
    {
        $this->checkConstructorArgument($number);
        $this->number = $number;
        $this->scale = null;
    }

    public function add(INumber $number)
    {
        return $this->binaryOperator('bcadd', $number);
    }

    public function subtract(INumber $number)
    {
        return $this->binaryOperator('bcsub', $number);
    }

    public function multiply(INumber $number)
    {
        return $this->binaryOperator('bcmul', $number);
    }

    public function divide(INumber $number)
    {
        $this->checkNumberIsNotZero($number);

        return $this->binaryOperator('bcdiv', $number);
    }

    public function quotient(INumber $number)
    {
        return $this->divide($number)->roundDown();
    }

    public function modulo(INumber $number)
    {
        $this->checkNumberIsNotZero($number);
        $modulo = $this->getRealNumberModulo($number);
        if ($this->isNegative()) {
            return $modulo->negate();
        }
        return $modulo;
    }

    private function getRealNumberModulo($number)
    {
        $quotient = $this->quotient($number)->abs();

        return $this->abs()->subtract($number->abs()->multiply($quotient));
    }

    public function power(INumber $number)
    {
        if ($number->isInteger()) {
            return $this->binaryOperator('bcpow', $number->round(0)); // round because integer can be 2.00
        } elseif ($number->isEqual(new Number('0.5'))) {
            return $this->sqrt();
        } else {
            return $this->powerOfDecimal($number);
        }
    }

    private function powerOfDecimal(INumber $number)
    {
        $floatResult = pow((float) $this->__toString(), (float) $number->__toString());
        return new Number((string) $floatResult);
    }

    public function sqrt()
    {
        if ($this->isNegative()) {
            throw new \InvalidArgumentException();
        }
        return $this->unaryOperator('bcsqrt');
    }

    public function abs()
    {
        if ($this->isNegative()) {
            return $this->negate();
        }
        return clone $this;
    }

    public function negate()
    {
        return $this->multiply(new Number('-1'));
    }

    public function isInteger()
    {
        return !$this->hasDecimalPoint()
                || preg_match("~\.[0]+$~", $this)
                || substr($this, -1) == '.';
    }

    private function hasDecimalPoint()
    {
        return strpos($this, '.') !== false;
    }

    public function isZero()
    {
        return $this->isEqual(new Number());
    }

    public function isNegative()
    {
        return $this->isSmallerThan(new Number());
    }

    public function isPositive()
    {
        return $this->isBiggerThan(new Number());
    }

    public function compare(INumber $number)
    {
        return bccomp((string) $this, (string) $number);
    }

    public function isEqual(INumber $number)
    {
        return $this->compare($number) == 0;
    }

    public function isBiggerThan(INumber $number)
    {
        return $this->compare($number) == 1;
    }

    public function isSmallerThan(INumber $number)
    {
        return $this->compare($number) == -1;
    }

    public function round($precision)
    {
        $precision = $this->transformPrecision($precision);
        $this->checkPrecision($precision);
        if ($precision >= 0) {
            return $this->roundNumberAfterDecimalPoint($precision);
        } else {
            return $this->roundNumberBeforeDecimalPoint($precision);
        }
    }

    private function transformPrecision($precision)
    {
        if ($precision instanceof INumber) {
            return intval($precision->round(0)->__toString());
        }
        return $precision;
    }

    private function roundNumberAfterDecimalPoint($precision)
    {
        if (!$this->isInteger()) {
            $operation = $this->isPositive() ? 'bcadd' : 'bcsub';
            $number = $operation((string) $this, '0.' . str_repeat('0', $precision) . '5', $precision);
        } elseif ($this->hasDecimalPoint()) {
            $number = substr($this, 0, strpos($this, '.'));
        } else {
            $number = $this->__toString();
        }
        return new Number($number);
    }

    private function roundNumberBeforeDecimalPoint($precision)
    {
        $powerOfTen = - pow(10, abs($precision));
        return $this->roundToNumber((int) $powerOfTen);
    }

    public function roundUp()
    {
        if ($this->isInteger()) {
            return $this->round(0);
        }
        return $this->roundDown()->getIntegerFartherOneFromZero();
    }

    private function getIntegerFartherOneFromZero()
    {
        $addedNumber = $this->isNegative() ? '-1' : '1';
        $one = new Number($addedNumber);
        return $this->add($one)->round(0);
    }

    public function roundDown()
    {
        if ($this->isInteger()) {
            return $this->round(0);
        } elseif ($this->isNegative()) {
            return $this->getNegativeNumberClosestToZero();
        } else {
            return $this->getPositiveNumberClosestToZero();
        }
    }

    private function getNegativeNumberClosestToZero()
    {
        return $this->getPositiveNumberClosestToZero()->negate();
    }

    private function getPositiveNumberClosestToZero()
    {
        $number = bcadd((string) $this->abs(), 0, 0);
        return new Number($number);
    }

    public function roundToNumber($precision)
    {
        $precision = $this->transformPrecision($precision);
        $this->checkPrecision($precision);
        $absolutePrecision = new Number((string) abs($precision));
        if ($precision < 0) {
            return $this->getRoundByNearestMultiple($absolutePrecision);
        } elseif ($precision > 0 && !$this->isInteger()) {
            return $this->roundDecimalToNumber($absolutePrecision);
        } else {
            return $this->round(0);
        }
    }

    private function roundDecimalToNumber($absolutePrecision)
    {
        $precision = strlen($absolutePrecision);
        $fractions = $this->round($precision)->getFractionalPartAsNewNumber($precision);
        $modulo = $fractions->getRoundByNearestMultiple($absolutePrecision)->round(0);
        return new Number("{$this->roundDown()}.{$modulo}");
    }

    private function getFractionalPartAsNewNumber($precision)
    {
        $decimalNumber = $this->subtract($this->roundDown())->abs(); // e.g. 0.851
        $fractionalPart = substr($decimalNumber, 2, $precision); // e.g. 851
        return new Number($fractionalPart);
    }

    private function getRoundByNearestMultiple($absoluteMultiple)
    {
        $moduloDown = $this->modulo($absoluteMultiple)->abs();
        $moduloUp = $absoluteMultiple->subtract($moduloDown);
        if ($moduloDown->compare($moduloUp) >= 0) {
            $operation = $this->isPositive() ? 'add' : 'subtract';
            return $this->$operation($moduloUp);
        } else {
            $operation = $this->isPositive() ? 'subtract' : 'add';
            return $this->$operation($moduloDown);
        }
    }

    public function setLocalScale($scale)
    {
        $this->checkScale($scale);
        $this->scale = $scale;
    }

    public function resetLocalScale()
    {
        $this->scale = null;
    }

    public function __toString()
    {
        return $this->number;
    }

    public function __clone()
    {
        return new Number($this->number); // same as implicit clone in PHP (it's a unnecessary)
    }

    private function binaryOperator($callback, $number)
    {
        return $this->callBCMath($callback, (string) $this, (string) $number);
    }

    private function unaryOperator($callback)
    {
        return $this->callBCMath($callback, (string) $this);
    }

    /**
     * Parameters:
     * - first parameter is name of function from BC Math
     * - next parameters are parameters for function
     * It adds scale as last parameter if local scale is defined
     */
    private function callBCMath()
    {
        $parameters = func_get_args();
        $callback = array_shift($parameters);
        if ($this->isDefinedLocalScale()) {
            $parameters[] = $this->scale;
        }
        $resultString = call_user_func_array($callback, $parameters);
        return new Number($resultString);
    }

    private function isDefinedLocalScale()
    {
        return $this->scale !== null;
    }

    private function checkNumberIsNotZero($number)
    {
        if ($number->isEqual(new Number())) {
            throw new \InvalidArgumentException();
        }
    }

    private function checkConstructorArgument($value)
    {
        if (!is_string($value) || !is_numeric($value)) {
            throw new \InvalidArgumentException();
        }
    }

    private function checkPrecision($precision)
    {
        if (!is_int($precision)) {
            throw new \InvalidArgumentException();
        }
    }

    private function checkScale($scale)
    {
        if (!is_int($scale) || $scale < 0) {
            throw new \InvalidArgumentException();
        }
    }
}

<?php

namespace BN\Compiler\Token;

class TokenType
{
    const UNKNOWN = 'UNKNOWN';
    const ASSIGN = 'ASSIGN';
    const NUMBER = 'NUMBER';
    const CONSTANT = 'CONSTANT';
    const OPERATOR = 'OPERATOR';
    const VARIABLE = 'VARIABLE';
    const BRACKET_OPENING = 'BRACKET_OPENING';
    const BRACKET_CLOSING = 'BRACKET_CLOSING';
}

<?php

namespace BN\Compiler;

use BN\Compiler\Grammar\GrammarBuilder;

class EvaluatorInteractor
{
    private $scanner;
    private $parser;
    private $postfix;
    private $responder;

    public function __construct(GrammarBuilder $grammar)
    {
        $operators = $grammar->buildOperators();

        $this->responder = new ResponderDecorator();
        $this->scanner = $grammar->buildScanner();
        $this->parser = new Parser\ShuntingYardParser($this->responder, $operators);
        $this->postfix = new Postfix\PostfixEvaluator(
            $this->responder,
            $operators,
            $grammar->buildConstants()
        );
    }

    public function __invoke($input, \stdClass $variables, EvaluatorResponder $responder)
    {
        try {
            $this->evaluate($input, $variables, $responder);
        } catch (\Exception $e) {
            $this->responder->exception($e);
        }
    }

    private function evaluate($input, \stdClass $variables, EvaluatorResponder $responder)
    {
        $this->responder->setResponder($responder);
        $statements = $this->scanner->tokenize($input);
        foreach ($statements as $s) {
            $this->responder->nextStatement($s->statement);
            $queue = $this->parser->parse($s->tokens);
            if ($queue) {
                $result = $this->postfix->evaluate($queue, $variables);
                $this->responder->result($result);
            }
        }
    }
}

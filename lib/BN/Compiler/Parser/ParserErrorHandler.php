<?php

namespace BN\Compiler\Parser;

interface ParserErrorHandler
{
    public function mismatchedBrackets($mismatchedBracket);

    public function unknownOperator($operator);

    public function unknownToken($token);
}

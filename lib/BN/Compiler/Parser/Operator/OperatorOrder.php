<?php

namespace BN\Compiler\Parser\Operator;

class OperatorOrder
{
    private $precedence;
    private $isLeftAssociative;

    public function __construct($precedence, $isLeftAssociative)
    {
        $this->precedence = $precedence;
        $this->isLeftAssociative = $isLeftAssociative;
    }

    public function isNotPreceding(OperatorOrder $operator)
    {
        return ($this->isLeftAssociative && $this->precedence <= $operator->precedence)
            || $this->precedence < $operator->precedence;
    }
}

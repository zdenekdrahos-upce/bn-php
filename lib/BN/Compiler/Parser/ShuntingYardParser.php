<?php

namespace BN\Compiler\Parser;

use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;
use BN\Compiler\Grammar\Operators;
use BN\Compiler\Parser\Token\ParseValue;
use BN\Compiler\Parser\Token\ParseOperator;
use BN\Compiler\Parser\Token\ParseOpeningBracket;
use BN\Compiler\Parser\Token\ParseClosingBracket;
use BN\Compiler\Parser\Token\ParseUnknownToken;

class ShuntingYardParser
{
    private $accumulator;
    private $parsers;
    private $postfix;
    private $tokensArray;

    public function __construct(ParserErrorHandler $errorHandler, Operators $operators)
    {
        $this->accumulator = new QueueAccumulator($errorHandler);
        $this->postfix = new StackCleaning($this->accumulator);
        $parseValue = new ParseValue($this->accumulator);
        $parseOperator = new ParseOperator($this->accumulator, $operators);
        $this->parsers = array(
            TokenType::NUMBER => $parseValue,
            TokenType::VARIABLE => $parseValue,
            TokenType::CONSTANT => $parseValue,
            TokenType::ASSIGN => $parseOperator,
            TokenType::OPERATOR => $parseOperator,
            TokenType::BRACKET_OPENING => new ParseOpeningBracket($this->accumulator),
            TokenType::BRACKET_CLOSING => new ParseClosingBracket($this->accumulator),
            TokenType::UNKNOWN => new ParseUnknownToken($this->accumulator)
        );
    }

    public function parse(array $tokensInInfixNotation)
    {
        $this->prepareParser($tokensInInfixNotation);
        while ($this->existsUnreadToken()) {
            $this->readNextToken();
        }
        $this->pushOperatorsFromStack();
        return $this->getTokensInPostfixNotation();
    }

    private function prepareParser($tokens)
    {
        $this->tokensArray = $tokens;
        $this->accumulator->init();
    }

    private function existsUnreadToken()
    {
        return !empty($this->tokensArray);
    }

    private function readNextToken()
    {
        $token = array_shift($this->tokensArray);
        $parser = $this->getParser($token);
        $parser->parse($token);
    }

    private function getParser(Token $token)
    {
        if (array_key_exists($token->type, $this->parsers)) {
            return $this->parsers[$token->type];
        } else {
            return $this->parsers[TokenType::UNKNOWN];
        }
    }

    private function pushOperatorsFromStack()
    {
        $this->postfix->pushOperatorsFromStack();
    }

    private function getTokensInPostfixNotation()
    {
        return $this->accumulator->getResult();
    }
}

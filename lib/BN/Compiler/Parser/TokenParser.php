<?php

namespace BN\Compiler\Parser;

use BN\Compiler\Token\Token;

abstract class TokenParser
{
    /** @var \BN\Compiler\Parser\QueueAccumulator */
    protected $accumulator;

    public function __construct(QueueAccumulator $accumulator)
    {
        $this->accumulator = $accumulator;
    }

    abstract public function parse(Token $token);
}

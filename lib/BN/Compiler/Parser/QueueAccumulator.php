<?php

namespace BN\Compiler\Parser;

use BN\Collections\Stack;
use BN\Collections\Queue;
use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;

class QueueAccumulator
{
    private $stack;
    private $queue;
    private $errorHandler;
    private $continueInParsing;

    public function __construct(ParserErrorHandler $errorHandler)
    {
        $this->errorHandler = $errorHandler;
        $this->stack = new Stack();
    }

    public function init()
    {
        $this->continueInParsing = true;
        $this->queue = new Queue();
        $this->stack->clear();
    }

    public function noError()
    {
        return $this->continueInParsing;
    }

    public function pushTokenToStack(Token $token)
    {
        $this->stack->push($token);
    }

    public function popTokenFromStack()
    {
        return $this->stack->pop();
    }

    public function isStackEmpty()
    {
        return $this->stack->isEmpty();
    }

    public function isOperatorAtTopOfStack()
    {
        return $this->isTypeAtStackPeek(TokenType::OPERATOR)
            || $this->isTypeAtStackPeek(TokenType::ASSIGN);
    }

    public function isTypeAtStackPeek($tokenType)
    {
        return $this->stack->peek()->type == $tokenType;
    }

    public function valueAtStackPeek()
    {
        return $this->stack->peek()->value;
    }

    public function pushTokenFromStackToQueue()
    {
        $this->pushTokenToQueue($this->stack->pop());
    }

    public function pushTokenToQueue(Token $token)
    {
        $this->queue->push($token);
    }

    public function stopParsing($method, $arg = null)
    {
        $this->stack->clear();
        $this->continueInParsing = false;
        $this->errorHandler->$method($arg);
    }

    public function getResult()
    {
        if ($this->noError()) {
            return $this->queue;
        } else {
            return false;
        }
    }
}

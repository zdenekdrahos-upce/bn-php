<?php

namespace BN\Compiler\Parser\Token;

use BN\Compiler\Parser\TokenParser;
use BN\Compiler\Token\Token;

class ParseOpeningBracket extends TokenParser
{
    public function parse(Token $token)
    {
        $this->accumulator->pushTokenToStack($token);
    }
}

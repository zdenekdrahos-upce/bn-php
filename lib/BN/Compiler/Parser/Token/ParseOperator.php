<?php

namespace BN\Compiler\Parser\Token;

use BN\Compiler\Parser\TokenParser;
use BN\Compiler\Token\Token;
use BN\Compiler\Grammar\Operators;

class ParseOperator extends TokenParser
{
    private $operators = array();

    public function __construct($accumulator, Operators $operators)
    {
        $this->accumulator = $accumulator;
        $this->operators = $operators;
    }

    public function parse(Token $token)
    {
        $operatorSymbol = $token->value;
        if ($this->operators->exists($operatorSymbol)) {
            $operator = $this->operators->getOrder($operatorSymbol);
            while ($this->isOperatorAtTopOfStack()) {
                $stackOperator = $this->operators->getOrder($this->accumulator->valueAtStackPeek());
                if ($operator->isNotPreceding($stackOperator)) {
                    $this->accumulator->pushTokenFromStackToQueue();
                } else {
                    break;
                }
            }
            $this->accumulator->pushTokenToStack($token);
        } else {
            $this->accumulator->stopParsing('unknownOperator', $operatorSymbol);
        }
    }

    private function isOperatorAtTopOfStack()
    {
        return !$this->accumulator->isStackEmpty() && $this->accumulator->isOperatorAtTopOfStack();
    }
}

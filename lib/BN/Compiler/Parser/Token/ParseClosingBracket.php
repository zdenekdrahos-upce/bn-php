<?php

namespace BN\Compiler\Parser\Token;

use BN\Compiler\Parser\TokenParser;
use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;

class ParseClosingBracket extends TokenParser
{
    public function parse(Token $token)
    {
        while ($this->isNotLeftBracketAtTopOfStack()) {
            if ($this->accumulator->isStackEmpty()) {
                $this->accumulator->stopParsing('mismatchedBrackets', $token->value);
                return;
            }
            $this->accumulator->pushTokenFromStackToQueue();
        }
        $this->accumulator->popTokenFromStack();
    }

    private function isNotLeftBracketAtTopOfStack()
    {
        return $this->accumulator->isStackEmpty()
            || !$this->accumulator->isTypeAtStackPeek(TokenType::BRACKET_OPENING);
    }
}

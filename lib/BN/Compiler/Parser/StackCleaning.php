<?php

namespace BN\Compiler\Parser;

use BN\Compiler\Token\TokenType;

class StackCleaning
{
    private $accumulator;

    public function __construct(QueueAccumulator $accumulator)
    {
        $this->accumulator = $accumulator;
    }

    public function pushOperatorsFromStack()
    {
        while (!$this->accumulator->isStackEmpty()) {
            if ($this->accumulator->isOperatorAtTopOfStack()) {
                $this->accumulator->pushTokenFromStackToQueue();
            } elseif ($this->isBracketAtTopOfStack()) {
                $bracket = $this->accumulator->valueAtStackPeek();
                $this->accumulator->stopParsing('mismatchedBrackets', $bracket);
                return;
            }
        }
    }

    private function isBracketAtTopOfStack()
    {
        return $this->accumulator->isTypeAtStackPeek(TokenType::BRACKET_OPENING)
            || $this->accumulator->isTypeAtStackPeek(TokenType::BRACKET_CLOSING);
    }
}

<?php

namespace BN\Compiler\Grammar;

class Operators
{
    private $operators = array();

    public function register($symbol, OperatorBuilder $builder)
    {
        $this->operators[$symbol]['order'] = $builder->buildOrder();
        $this->operators[$symbol]['evaluator'] = $builder->buildEvaluator();
        $this->operators[$symbol]['operands'] = $builder->buildOperands();
    }

    public function exists($operatorSymbol)
    {
        return array_key_exists($operatorSymbol, $this->operators);
    }

    /** @return \BN\Compiler\Parser\Operator\OperatorOrder */
    public function getOrder($operatorSymbol)
    {
        return $this->operators[$operatorSymbol]['order'];
    }

    /** @return \BN\Compiler\Postfix\Operands\Operands */
    public function getOperands($operatorSymbol)
    {
        return $this->operators[$operatorSymbol]['operands'];
    }

    /** @return \BN\Compiler\Postfix\Operator\OperatorEvaluator */
    public function getEvaluator($operatorSymbol)
    {
        return $this->operators[$operatorSymbol]['evaluator'];
    }
}

<?php

namespace BN\Compiler\Grammar;

use BN\Compiler\Parser\Operator\OperatorOrder;
use BN\Compiler\Postfix\Operator\CallbackEvaluator;
use BN\Compiler\Postfix\Operands\FixedCount;
use BN\Compiler\Postfix\Operands\AtLeastN;

class OperatorBuilder
{
    private $precedence;
    private $isLeftAssociative;
    private $operands;
    private $evaluator;

    public function precedence($precedence)
    {
        $this->precedence = $precedence;
        return $this;
    }

    public function leftAssociative()
    {
        $this->isLeftAssociative = true;
        return $this;
    }

    public function rightAssociative()
    {
        $this->isLeftAssociative = false;
        return $this;
    }

    public function unary($callback)
    {
        return $this->operator(1, $callback);
    }

    public function binary($callback)
    {
        return $this->operator(2, $callback);
    }

    public function operator($fixedOperandsCount, $callback)
    {
        $this->operands = new FixedCount($fixedOperandsCount);
        $this->evaluator = new CallbackEvaluator($callback);
        return $this;
    }

    public function aggregate($callback)
    {
        $this->operands = new AtLeastN(1);
        $this->evaluator = $callback;
        return $this;
    }

    public function buildOrder()
    {
        return new OperatorOrder($this->precedence, $this->isLeftAssociative);
    }

    public function buildEvaluator()
    {
        return $this->evaluator;
    }

    public function buildOperands()
    {
        return $this->operands;
    }
}

<?php

namespace BN\Compiler\Grammar;

use BN\Compiler\Token\TokenType;
use BN\Compiler\Scanner\Scanner;
use BN\Compiler\Scanner\UnarySignsScanner;
use BN\Compiler\Scanner\LexemeToToken;
use BN\Compiler\Scanner\LexemeToTokens;
use BN\Compiler\Scanner\LexemeConverter;
use BN\Compiler\Scanner\Converter\LexemeToUnknown;
use BN\Compiler\Scanner\Converter\LexemeToKeyword;
use BN\Compiler\Scanner\Converter\LexemeToConstant;
use BN\Compiler\Scanner\Converter\LexemeToAssign;
use BN\Compiler\Scanner\Converter\LexemeToVariable;

class GrammarBuilder
{
    private $toNumber;
    private $keywords;
    private $constants;
    private $operators;
    private $unarySigns;
    private $whiteSpace;
    private $assignOperator;
    private $statementsSeparator;

    public function __construct(LexemeConverter $lexemeToNumber)
    {
        $this->toNumber = $lexemeToNumber;
        $this->keywords = array();
        $this->whiteSpace = array();
        $this->constants = array();
        $this->operators = new Operators();
    }

    public function statementsSeparator($statementsSeparator)
    {
        $this->statementsSeparator = $statementsSeparator;
        return $this;
    }

    public function whiteSpace()
    {
        $this->whiteSpace = func_get_args();
        return $this;
    }

    public function assign($assignOperator)
    {
        $operator = new OperatorBuilder();
        $this->assignOperator = $assignOperator;
        $this->operators->register(
            $assignOperator,
            $operator->precedence(0)->rightAssociative()
        );
        return $this;
    }

    public function brackets($openingBracket, $closingBracket)
    {
        $this->keyword($openingBracket, TokenType::BRACKET_OPENING);
        $this->keyword($closingBracket, TokenType::BRACKET_CLOSING);
        return $this;
    }

    public function unarySigns()
    {
        $this->unarySigns = func_get_args();
        return $this;
    }

    public function operators(array $operators)
    {
        foreach ($operators as $symbol => $operator) {
            $this->operators->register($symbol, $operator);
            $this->keyword($symbol, TokenType::OPERATOR);
        }
        return $this;
    }

    public function keyword($symbol, $tokenType)
    {
        $this->keywords[$symbol] = $tokenType;
        return $this;
    }

    public function numberConstant($symbol, $value)
    {
        $this->constants[$symbol] = $value;
        return $this;
    }

    public function buildOperators()
    {
        return $this->operators;
    }

    public function buildConstants()
    {
        return $this->constants;
    }

    public function buildScanner()
    {
        $unknownLexeme = new LexemeToUnknown();
        $toToken = new LexemeToToken($unknownLexeme);
        $toToken->registerLexer(new LexemeToAssign($this->assignOperator));
        $toToken->registerLexer(new LexemeToVariable());
        $toToken->registerLexer($this->toNumber);
        $toToken->registerLexer(new LexemeToKeyword($this->keywords));
        $toToken->registerLexer(new LexemeToConstant(array_keys($this->constants)));
        $toTokens = new LexemeToTokens($toToken);
        $scanner = new Scanner($toTokens, $this->whiteSpace, $this->statementsSeparator);
        if ($this->unarySigns) {
            return new UnarySignsScanner($scanner, $this->unarySigns);
        } else {
            return $scanner;
        }
    }
}

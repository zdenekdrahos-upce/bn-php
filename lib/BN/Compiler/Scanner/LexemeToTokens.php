<?php

namespace BN\Compiler\Scanner;

class LexemeToTokens
{
    private $converter;
    private $lexeme;
    private $lexemeLength;
    private $lastToken;

    public function __construct(LexemeToToken $toToken)
    {
        $this->converter = $toToken;
    }

    public function lexemeToTokens($lexeme)
    {
        $tokens = array();
        $this->initLexeme($lexeme);
        while ($this->hasLexemeAtLeastOneCharacter()) {
            $this->lexemeToToken();
            $this->makeLexemeShorter();
            $tokens[] = $this->lastToken;
        }
        return $tokens;
    }

    private function initLexeme($lexeme)
    {
        $this->lexeme = $lexeme;
        $this->lexemeLength = mb_strlen($this->lexeme, 'UTF-8');
    }

    private function hasLexemeAtLeastOneCharacter()
    {
        return $this->lexemeLength > 0;
    }

    private function lexemeToToken()
    {
        $this->lastToken = $this->converter->lexemeToToken($this->lexeme);
    }

    private function makeLexemeShorter()
    {
        $tokenLength = mb_strlen($this->lastToken->value, 'UTF-8');
        $this->lexeme = mb_substr($this->lexeme, $tokenLength, $this->lexemeLength, 'UTF-8');
        $this->lexemeLength -= $tokenLength;
    }
}

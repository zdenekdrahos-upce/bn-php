<?php

namespace BN\Compiler\Scanner;

class Scanner
{
    private $converter;
    private $whitespace;
    private $statementsSeparator;

    public function __construct(LexemeToTokens $converter, array $whitespace, $statementsSeparator)
    {
        $this->converter = $converter;
        $this->whitespace = $whitespace;
        $this->statementsSeparator = $statementsSeparator;
    }

    public function tokenize($text)
    {
        $statements = array();
        foreach (explode($this->statementsSeparator, $text) as $statement) {
            $tokens = $this->statementToTokens($statement);
            if ($tokens) {
                $statements[] = new Statement($statement, $tokens);
            }
        }
        return $statements;
    }

    private function statementToTokens($statement)
    {
        $tokens = array();
        $lexemes = $this->inputToLexemes($statement);
        foreach ($lexemes as $lexeme) {
            $tokens = array_merge(
                $tokens,
                $this->converter->lexemeToTokens($lexeme)
            );
        }
        return $tokens;
    }

    private function inputToLexemes($input)
    {
        return explode(
            ' ',
            $this->replaceWhitespaceWithSpace($input)
        );
    }

    private function replaceWhitespaceWithSpace($input)
    {
        return str_replace($this->whitespace, ' ', $input);
    }
}

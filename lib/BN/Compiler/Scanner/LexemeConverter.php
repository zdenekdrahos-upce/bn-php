<?php

namespace BN\Compiler\Scanner;

interface LexemeConverter
{

    /**
     * @param string $lexeme
     * @return bool
     */
    public function canConvertLexeme($lexeme);

    /**
     * @param string $lexeme
     * @return \BN\Compiler\Token\Token
     */
    public function convertLexeme($lexeme);
}

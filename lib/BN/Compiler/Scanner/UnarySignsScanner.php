<?php

namespace BN\Compiler\Scanner;

class UnarySignsScanner
{
    private $scanner;
    private $unarySigns;

    public function __construct(Scanner $scanner, array $unarySigns)
    {
        $this->scanner = $scanner;
        $this->unarySigns = $unarySigns;
    }

    public function tokenize($text)
    {
        $fixed = $this->addSpaceAroundSigns($text);
        return $this->scanner->tokenize($fixed);
    }

    private function addSpaceAroundSigns($text)
    {
        $replaced = $text;
        foreach ($this->unarySigns as $sign) {
            $replaced = preg_replace("~(?<=[0-9])\\{$sign}(?=[0-9])~", " {$sign} ", $replaced);
        }
        return $replaced;
    }
}

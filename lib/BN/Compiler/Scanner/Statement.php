<?php

namespace BN\Compiler\Scanner;

class Statement
{
    public $statement;
    public $tokens;

    public function __construct($statement, array $tokens)
    {
        $this->statement = $statement;
        $this->tokens = $tokens;
    }
}

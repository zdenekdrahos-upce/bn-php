<?php

namespace BN\Compiler\Scanner;

class LexemeToToken
{
    private $lexers = array();
    private $unknown;

    public function __construct(LexemeConverter $unknownLexeme)
    {
        $this->unknown = $unknownLexeme;
    }

    public function registerLexer(LexemeConverter $lexer)
    {
        $this->lexers[] = $lexer;
    }

    public function lexemeToToken($originalLexeme)
    {
        $lexeme = $originalLexeme;
        while ($lexeme !== '') {
            foreach ($this->lexers as $lexer) {
                if ($lexer->canConvertLexeme($lexeme)) {
                    return $lexer->convertLexeme($lexeme);
                }
            }
            $lexeme = mb_substr($lexeme, 0, -1, 'UTF-8');
        }
        return $this->unknown->convertLexeme($originalLexeme);
    }
}

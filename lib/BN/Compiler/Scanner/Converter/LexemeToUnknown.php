<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;
use BN\Compiler\Scanner\LexemeConverter;

class LexemeToUnknown implements LexemeConverter
{

    public function canConvertLexeme($lexeme)
    {
        return true;
    }

    public function convertLexeme($lexeme)
    {
        return new Token(TokenType::UNKNOWN, $lexeme);
    }
}

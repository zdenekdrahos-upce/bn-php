<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Scanner\LexemeConverter;
use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;

class LexemeToConstant implements LexemeConverter
{
    private $constants = array();

    public function __construct(array $constants)
    {
        $this->constants = $constants;
    }

    public function canConvertLexeme($lexeme)
    {
        return in_array($lexeme, $this->constants, true);
    }

    public function convertLexeme($lexeme)
    {
        return new Token(TokenType::CONSTANT, $lexeme);
    }
}

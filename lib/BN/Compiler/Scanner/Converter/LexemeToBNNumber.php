<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Number;
use BN\Compiler\Scanner\LexemeConverter;

class LexemeToBNNumber implements LexemeConverter
{
    private $toNumber;

    public function __construct()
    {
        $this->toNumber = new LexemeToSignedNumber();
    }

    public function canConvertLexeme($lexeme)
    {
        return $this->toNumber->canConvertLexeme($lexeme);
    }

    public function convertLexeme($lexeme)
    {
        $token = $this->toNumber->convertLexeme($lexeme);
        $token->value = new Number($token->value);
        return $token;
    }
}

<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;
use BN\Compiler\Scanner\LexemeConverter;

class LexemeToVariable implements LexemeConverter
{
    public function canConvertLexeme($lexeme)
    {
        $startsWithDollar = $lexeme[0] == '$';
        $alphaCharacters = ctype_alpha(substr($lexeme, 1));
        return $startsWithDollar && $alphaCharacters;
    }

    public function convertLexeme($lexeme)
    {
        return new Token(TokenType::VARIABLE, $lexeme);
    }
}

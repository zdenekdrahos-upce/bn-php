<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\Token;
use BN\Compiler\Scanner\LexemeConverter;

class LexemeToKeyword implements LexemeConverter
{
    private $keywords;

    public function __construct(array $keywords)
    {
        $this->keywords = $keywords;
    }

    public function canConvertLexeme($lexeme)
    {
        return array_key_exists($lexeme, $this->keywords);
    }

    public function convertLexeme($lexeme)
    {
        return new Token($this->keywords[$lexeme], $lexeme);
    }
}

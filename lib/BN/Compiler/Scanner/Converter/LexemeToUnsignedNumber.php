<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;
use BN\Compiler\Scanner\LexemeConverter;

class LexemeToUnsignedNumber implements LexemeConverter
{
    public function canConvertLexeme($lexeme)
    {
        return $this->isStringNumeric($lexeme)
            && $this->noUnarySign($lexeme);
    }

    private function isStringNumeric($lexeme)
    {
        return is_string($lexeme) && is_numeric($lexeme);
    }

    private function noUnarySign($lexeme)
    {
        return !in_array($lexeme[0], array('+', '-'), true);
    }

    public function convertLexeme($lexeme)
    {
        return new Token(TokenType::NUMBER, $lexeme);
    }
}

<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;
use BN\Compiler\Scanner\LexemeConverter;

class LexemeToAssign implements LexemeConverter
{
    private $operator;

    public function __construct($operator)
    {
        $this->operator = $operator;
    }

    public function canConvertLexeme($lexeme)
    {
        return $lexeme == $this->operator;
    }

    public function convertLexeme($lexeme)
    {
        return new Token(TokenType::ASSIGN, $lexeme);
    }
}

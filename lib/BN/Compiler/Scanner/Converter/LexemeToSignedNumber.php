<?php

namespace BN\Compiler\Scanner\Converter;

use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;
use BN\Compiler\Scanner\LexemeConverter;

class LexemeToSignedNumber implements LexemeConverter
{
    public function canConvertLexeme($lexeme)
    {
        return $this->isStringNumeric($lexeme)
            || $this->numberWithUnarySign($lexeme);
    }

    private function isStringNumeric($lexeme)
    {
        return is_string($lexeme) && is_numeric($lexeme);
    }

    private function numberWithUnarySign($lexeme)
    {
        $afterSign = substr($lexeme, 1);
        return $this->startWithUnarySign($lexeme)
            && $this->isStringNumeric($afterSign)
            && $this->noUnarySign($afterSign);
    }

    private function noUnarySign($lexeme)
    {
        return !$this->startWithUnarySign($lexeme);
    }

    private function startWithUnarySign($lexeme)
    {
        return in_array($lexeme[0], array('+', '-'), true);
    }

    public function convertLexeme($lexeme)
    {
        return new Token(TokenType::NUMBER, $lexeme);
    }
}

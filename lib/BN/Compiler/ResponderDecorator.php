<?php

namespace BN\Compiler;

use BN\Compiler\Postfix\Operands\OperandsSummary;

class ResponderDecorator implements EvaluatorResponder
{
    private $responder;

    public function setResponder(EvaluatorResponder $responder)
    {
        $this->responder = $responder;
    }

    public function nextStatement($statement)
    {
        $this->responder->nextStatement($statement);
    }

    public function unknownOperator($operator)
    {
        $this->responder->unknownOperator($operator);
    }

    public function unknownToken($token)
    {
        $this->responder->unknownToken($token);
    }

    public function mismatchedBrackets($mismatchedBracket)
    {
        $this->responder->mismatchedBrackets($mismatchedBracket);
    }

    public function invalidOperands($operatorSymbol, OperandsSummary $operands)
    {
        $this->responder->invalidOperands($operatorSymbol, $operands);
    }

    public function missingOperator($expectedOperandsCount)
    {
        $this->responder->missingOperator($expectedOperandsCount);
    }

    public function undefinedVariable($variableName)
    {
        $this->responder->undefinedVariable($variableName);
    }

    public function result($result)
    {
        $this->responder->result($result);
    }

    public function exception(\Exception $e)
    {
        $this->responder->exception($e);
    }
}

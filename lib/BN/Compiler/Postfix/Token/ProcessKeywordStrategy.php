<?php

namespace BN\Compiler\Postfix\Token;

interface ProcessKeywordStrategy
{
    public function isFirstOperandVariable();

    public function getOperands();

    public function tokensToOperands(array $tokens);

    public function getEvaluator();
}

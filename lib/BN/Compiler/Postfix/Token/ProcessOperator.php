<?php

namespace BN\Compiler\Postfix\Token;

use BN\Compiler\Token\Token;
use BN\Compiler\Postfix\ProcessToken;
use BN\Compiler\Grammar\Operators;
use BN\Compiler\Postfix\Variables;

class ProcessOperator extends ProcessToken implements ProcessKeywordStrategy
{
    private $operators;
    private $operatorSymbol;
    private $processKeyword;

    public function __construct($accumulator, Operators $operators, Variables $variables)
    {
        $this->accumulator = $accumulator;
        $this->operators = $operators;
        $this->processKeyword = new ProcessKeyword($accumulator, $variables, $this);
    }

    public function process(Token $token)
    {
        $this->operatorSymbol = $token->value;
        if ($this->operators->exists($this->operatorSymbol)) {
            $this->processKeyword->process($token);
        } else {
            $this->accumulator->stopCalculation('unknownOperator', $this->operatorSymbol);
        }
    }

    public function isFirstOperandVariable()
    {
        return false;
    }

    public function getOperands()
    {
        return $this->operators->getOperands($this->operatorSymbol);
    }

    public function tokensToOperands(array $tokens)
    {
        $operands = array();
        foreach ($tokens as $token) {
            $operands[] = $token->value;
        }
        return $operands;
    }

    public function getEvaluator()
    {
        return $this->operators->getEvaluator($this->operatorSymbol);
    }
}

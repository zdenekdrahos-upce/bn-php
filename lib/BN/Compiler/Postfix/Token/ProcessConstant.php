<?php

namespace BN\Compiler\Postfix\Token;

use BN\Compiler\Token\Token;
use BN\Compiler\Postfix\ProcessToken;

class ProcessConstant extends ProcessToken
{
    private $constants;

    public function __construct($accumulator, array $constants)
    {
        $this->accumulator = $accumulator;
        $this->constants = $constants;
    }

    public function process(Token $token)
    {
        $constant = $token->value;
        if (array_key_exists($constant, $this->constants)) {
            $value = $this->constants[$constant];
            $this->accumulator->pushNumber($value);
        } else {
            $this->accumulator->stopCalculation('undefinedVariable', $constant);
        }
    }
}

<?php

namespace BN\Compiler\Postfix\Token;

use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;
use BN\Compiler\Postfix\Operands\Operands;
use BN\Compiler\Postfix\Operands\OperandsSummary;
use BN\Compiler\Postfix\Variables;

class ProcessKeyword
{
    private $variables;
    private $strategy;
    private $accumulator;

    public function __construct($accumulator, Variables $variables, ProcessKeywordStrategy $strategy)
    {
        $this->accumulator = $accumulator;
        $this->variables = $variables;
        $this->strategy = $strategy;
    }

    public function process(Token $token)
    {
        $operands = $this->strategy->getOperands();
        $operandsSummary = $this->getOperands($operands);

        if ($this->accumulator->continueInCalculation()) {
            if ($operandsSummary->areOperandsValid) {
                $result = $this->evaluate($operandsSummary);
                $this->accumulator->pushNumber($result);
            } else {
                $this->accumulator->stopCalculation('invalidOperands', $token->value, $operandsSummary);
            }
        }
    }

    private function getOperands(Operands $operands)
    {
        $summary = $operands->getOperands($this->accumulator->getStack());
        foreach ($summary->operands as $i => $token) {
            if ($token->type == TokenType::VARIABLE) {
                if ($i == 0 && $this->strategy->isFirstOperandVariable()) {
                    continue;
                } elseif ($this->variables->exists($token)) {
                    $token->value = $this->variables->get($token);
                    $summary->operands[$i] = $token;
                } else {
                    $this->accumulator->stopCalculation('undefinedVariable', $token->value);
                }
            }
        }
        return $summary;
    }

    private function evaluate(OperandsSummary $summary)
    {
        $evaluator = $this->strategy->getEvaluator();
        $operands = $this->strategy->tokensToOperands($summary->operands);
        return $evaluator($operands);
    }
}

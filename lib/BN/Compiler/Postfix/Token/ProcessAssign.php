<?php

namespace BN\Compiler\Postfix\Token;

use BN\Compiler\Token\Token;
use BN\Compiler\Postfix\ProcessToken;
use BN\Compiler\Postfix\Variables;
use BN\Compiler\Postfix\Operands\FixedCount;
use BN\Compiler\Postfix\Operator\AssignOperator;

class ProcessAssign extends ProcessToken implements ProcessKeywordStrategy
{
    private $evaluator;
    private $operands;
    private $processKeyword;

    public function __construct($accumulator, Variables $variables)
    {
        $this->operands = new FixedCount(2);
        $this->evaluator = new AssignOperator($variables);
        $this->processKeyword = new ProcessKeyword($accumulator, $variables, $this);
    }

    public function process(Token $token)
    {
        return $this->processKeyword->process($token);
    }

    public function isFirstOperandVariable()
    {
        return true;
    }

    public function getOperands()
    {
        return $this->operands;
    }

    public function tokensToOperands(array $tokens)
    {
        return $tokens;
    }

    public function getEvaluator()
    {
        return $this->evaluator;
    }
}

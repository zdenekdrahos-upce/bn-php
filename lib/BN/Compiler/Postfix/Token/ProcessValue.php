<?php

namespace BN\Compiler\Postfix\Token;

use BN\Compiler\Token\Token;
use BN\Compiler\Postfix\ProcessToken;

class ProcessValue extends ProcessToken
{
    public function process(Token $token)
    {
        $this->accumulator->pushToken($token);
    }
}

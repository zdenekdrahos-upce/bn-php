<?php

namespace BN\Compiler\Postfix;

use BN\Compiler\Token\Token;

class Variables
{
    private $object;

    public function __construct()
    {
        $this->object = new \stdClass();
    }

    public function setVariables(\stdClass $object)
    {
        $this->object = $object;
    }

    public function exists(Token $token)
    {
        $variable = $this->tokenToVariableName($token);
        return property_exists($this->object, $variable);
    }

    public function get(Token $token)
    {
        $variable = $this->tokenToVariableName($token);
        return $this->object->$variable;
    }

    public function set(Token $token, $value)
    {
        $variable = $this->tokenToVariableName($token);
        $this->object->$variable = $value;
    }

    private function tokenToVariableName($token)
    {
        return substr($token->value, 1);
    }
}

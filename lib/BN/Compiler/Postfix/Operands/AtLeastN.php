<?php

namespace BN\Compiler\Postfix\Operands;

use BN\Collections\Stack;

class AtLeastN extends Operands
{
    protected function popOperands(Stack $stack)
    {
        $operands = array();
        while (!$stack->isEmpty()) {
            $operands[] = $stack->pop();
        }
        return $operands;
    }

    protected function isOperandsCountValid($operandsCount)
    {
        return $operandsCount >= $this->operandsCount;
    }
}

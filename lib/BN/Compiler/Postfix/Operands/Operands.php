<?php

namespace BN\Compiler\Postfix\Operands;

use BN\Collections\Stack;

abstract class Operands
{
    protected $operandsCount;

    public function __construct($operandsCount)
    {
        $this->operandsCount = $operandsCount;
    }

    public function getOperands(Stack $stack)
    {
        $r = new OperandsSummary();
        $r->operands = $this->getOperandsInInfixOrder($stack);
        $r->areOperandsValid = $this->isOperandsCountValid($r->countOperands());
        $r->expectedCount = $this->operandsCount;
        return $r;
    }

    private function getOperandsInInfixOrder(Stack $stack)
    {
        $operands = $this->popOperands($stack);
        return array_reverse($operands);
    }

    abstract protected function popOperands(Stack $stack);

    abstract protected function isOperandsCountValid($operandsCount);
}

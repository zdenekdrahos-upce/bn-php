<?php

namespace BN\Compiler\Postfix\Operands;

use BN\Collections\Stack;

class FixedCount extends Operands
{
    protected function popOperands(Stack $stack)
    {
        $operands = array();
        $count = 0;
        while (!$stack->isEmpty()) {
            $operands[] = $stack->pop();
            if (++$count == $this->operandsCount) {
                break;
            }
        }
        return $operands;
    }

    protected function isOperandsCountValid($operandsCount)
    {
        return $operandsCount == $this->operandsCount;
    }
}

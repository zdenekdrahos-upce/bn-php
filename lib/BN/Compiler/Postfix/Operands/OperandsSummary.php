<?php

namespace BN\Compiler\Postfix\Operands;

class OperandsSummary
{
    /** @var bool */
    public $areOperandsValid;
    /** @var array */
    public $operands;
    /** @var int */
    public $expectedCount;

    public function countOperands()
    {
        return count($this->operands);
    }
}

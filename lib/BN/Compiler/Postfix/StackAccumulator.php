<?php

namespace BN\Compiler\Postfix;

use BN\Collections\Stack;
use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;

class StackAccumulator
{
    private $errorHandler;
    private $stack;
    private $continueInCalculation;

    public function __construct(CalculatorErrorHandler $errorHandler)
    {
        $this->errorHandler = $errorHandler;
        $this->stack = new Stack();
    }

    public function init()
    {
        $this->stack->clear();
        $this->continueInCalculation = true;
    }

    public function continueInCalculation()
    {
        return $this->continueInCalculation;
    }

    public function pushToken(Token $token)
    {
        $this->stack->push($token);
    }

    public function pushNumber($number)
    {
        $this->pushToken(
            new Token(TokenType::NUMBER, $number)
        );
    }

    public function stopCalculation()
    {
        $args = func_get_args();
        $method = array_shift($args);

        $this->continueInCalculation = false;
        call_user_func_array(array($this->errorHandler, $method), $args);
    }

    public function getStack()
    {
        return $this->stack;
    }

    public function getResult()
    {
        $size = $this->stack->size();
        if ($size == 1) {
            return $this->stack->pop()->value;
        } elseif ($size > 0) {
            $this->stopCalculation('missingOperator', $size);
        }
        return 0;
    }
}

<?php

namespace BN\Compiler\Postfix\Operator;

use BN\Compiler\Postfix\Variables;
use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;

class AssignOperator
{
    private $variables;

    public function __construct(Variables $variables)
    {
        $this->variables = $variables;
    }

    public function __invoke(array $tokens)
    {
        $a = array_shift($tokens);
        $b = array_shift($tokens);
        return $this->evaluate($a, $b);
    }

    public function evaluate(Token $a, Token $b)
    {
        if ($a->type == TokenType::VARIABLE) {
            $value = $this->variables->exists($b) ? $this->variables->get($b) : $b->value;
            $this->variables->set($a, $value);
            return $value;
        } else {
            return null;
        }
    }
}

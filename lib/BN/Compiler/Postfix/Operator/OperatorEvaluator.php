<?php

namespace BN\Compiler\Postfix\Operator;

interface OperatorEvaluator
{
    public function __invoke(array $operands);
}

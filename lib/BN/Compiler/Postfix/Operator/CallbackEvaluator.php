<?php

namespace BN\Compiler\Postfix\Operator;

class CallbackEvaluator implements OperatorEvaluator
{
    public $callback;

    public function __construct($callback)
    {
        $this->callback = $callback;
    }

    public function __invoke(array $operands)
    {
        return call_user_func_array($this->callback, $operands);
    }
}

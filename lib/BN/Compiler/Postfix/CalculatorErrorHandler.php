<?php

namespace BN\Compiler\Postfix;

use BN\Compiler\Postfix\Operands\OperandsSummary;

interface CalculatorErrorHandler
{
    public function unknownOperator($operator);

    public function unknownToken($token);

    public function invalidOperands($operatorSymbol, OperandsSummary $operands);

    public function missingOperator($expectedOperandsCount);

    public function undefinedVariable($variableName);
}

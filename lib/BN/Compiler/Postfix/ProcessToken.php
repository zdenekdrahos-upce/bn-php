<?php

namespace BN\Compiler\Postfix;

use BN\Compiler\Token\Token;

abstract class ProcessToken
{
    /** @var StackAccumulator */
    protected $accumulator;

    public function __construct(StackAccumulator $accumulator)
    {
        $this->accumulator = $accumulator;
    }

    abstract public function process(Token $token);
}

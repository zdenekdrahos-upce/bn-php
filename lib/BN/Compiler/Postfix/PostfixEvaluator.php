<?php

namespace BN\Compiler\Postfix;

use BN\Collections\Queue;
use BN\Compiler\Token\Token;
use BN\Compiler\Token\TokenType;
use BN\Compiler\Grammar\Operators;
use BN\Compiler\Postfix\Token\ProcessValue;
use BN\Compiler\Postfix\Token\ProcessOperator;
use BN\Compiler\Postfix\Token\ProcessUnknown;
use BN\Compiler\Postfix\Token\ProcessAssign;
use BN\Compiler\Postfix\Token\ProcessConstant;

class PostfixEvaluator
{
    private $accumulator;
    private $calculators;
    private $tokensQueue;
    private $variables;

    public function __construct(CalculatorErrorHandler $errorHandler, Operators $operators, array $constants)
    {
        $this->variables = new Variables();
        $this->accumulator = new StackAccumulator($errorHandler);
        $processValue = new ProcessValue($this->accumulator);
        $this->calculators = array(
            TokenType::NUMBER => $processValue,
            TokenType::VARIABLE => $processValue,
            TokenType::CONSTANT => new ProcessConstant($this->accumulator, $constants),
            TokenType::ASSIGN => new ProcessAssign($this->accumulator, $this->variables),
            TokenType::OPERATOR => new ProcessOperator($this->accumulator, $operators, $this->variables),
            TokenType::UNKNOWN => new ProcessUnknown($this->accumulator)
        );
    }

    public function evaluate(Queue $queue, \stdClass $variables)
    {
        $this->prepareEvaluator($queue, $variables);
        while ($this->isTokenInQueue()) {
            $this->readNextToken();
        }
        return $this->evaluateResult();
    }

    private function prepareEvaluator($queue, $variables)
    {
        $this->accumulator->init();
        $this->tokensQueue = $queue;
        $this->variables->setVariables($variables);
    }

    private function isTokenInQueue()
    {
        return !$this->tokensQueue->isEmpty() && $this->accumulator->continueInCalculation();
    }

    private function readNextToken()
    {
        $token = $this->tokensQueue->pop();
        $p = $this->getProcessor($token);
        $p->process($token);
    }

    private function getProcessor(Token $token)
    {
        if (array_key_exists($token->type, $this->calculators)) {
            return $this->calculators[$token->type];
        } else {
            return $this->calculators[TokenType::UNKNOWN];
        }
    }

    private function evaluateResult()
    {
        return $this->accumulator->getResult();
    }
}

<?php

namespace BN\Compiler;

use BN\Compiler\Parser\ParserErrorHandler;
use BN\Compiler\Postfix\CalculatorErrorHandler;

interface EvaluatorResponder extends ParserErrorHandler, CalculatorErrorHandler
{
    public function nextStatement($statement);

    public function result($result);

    public function exception(\Exception $e);
}

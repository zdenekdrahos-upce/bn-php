<?php

namespace BN;

use BN\Compiler\Grammar\OperatorBuilder;

class OperatorsFactory
{
    private $operators;
    private $aggregation;

    public function __construct()
    {
        $this->aggregation = new AggregateFunctions();
        $this->operators = array(
            'abs' => $this->unaryOperator('abs')->leftAssociative()->precedence(6),
            'neg' => $this->unaryOperator('negate')->leftAssociative()->precedence(6),
            'floor' => $this->unaryOperator('roundDown')->leftAssociative()->precedence(5),
            'ceil' => $this->unaryOperator('roundUp')->leftAssociative()->precedence(5),
            'round' => $this->binaryOperator('round')->rightAssociative()->precedence(5),
            'roundTo' => $this->binaryOperator('roundToNumber')->rightAssociative()->precedence(5),
            '^' => $this->binaryOperator('power')->rightAssociative()->precedence(4),
            'sqrt' => $this->unaryOperator('sqrt')->leftAssociative()->precedence(4),
            '*' => $this->binaryOperator('multiply')->leftAssociative()->precedence(3),
            '/' => $this->binaryOperator('divide')->leftAssociative()->precedence(3),
            '\\' => $this->binaryOperator('quotient')->leftAssociative()->precedence(3),
            '%' => $this->binaryOperator('modulo')->leftAssociative()->precedence(2),
            '+' => $this->binaryOperator('add')->leftAssociative()->precedence(2),
            '-' => $this->binaryOperator('subtract')->leftAssociative()->precedence(2),
            'sum' => $this->aggregateFunction('sum')->leftAssociative()->precedence(1),
            'count' => $this->aggregateFunction('count')->leftAssociative()->precedence(1),
            'avg' => $this->aggregateFunction('avg')->leftAssociative()->precedence(1),
            'min' => $this->aggregateFunction('min')->leftAssociative()->precedence(1),
            'max' => $this->aggregateFunction('max')->leftAssociative()->precedence(1)
        );
    }

    public function create($symbol)
    {
        return $this->operators[$symbol];
    }

    public function getAll()
    {
        return $this->operators;
    }

    private function binaryOperator($method)
    {
        $operator = new OperatorBuilder();
        return $operator
            ->binary(function ($a, $b) use ($method) {
                return $a->$method($b);
            });
    }

    private function unaryOperator($method)
    {
        $operator = new OperatorBuilder();
        return $operator
            ->unary(function ($a) use ($method) {
                return $a->$method();
            });
    }

    private function aggregateFunction($method)
    {
        $operator = new OperatorBuilder();
        return $operator->aggregate(array($this->aggregation, $method));
    }
}

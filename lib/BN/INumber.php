<?php
/**
 * BN-PHP (https://bitbucket.org/zdenekdrahos/bn-php)
 * @license New BSD License
 * @author Zdenek Drahos
 */

namespace BN;

/**
 * Interface for number which is used in expression evaluator. Variables can be
 * used in evaluator only if they implement this interface. In class which implements
 * this interface number have to be represented as string and the representation
 * is accessible via @method __toString().
 */
interface INumber
{
    /**
     * Gets a new number which is the sum of instance and argument.
     * @param  \BN\INumber $number
     * @return \BN\INumber
     */
    public function add(INumber $number);

    /**
     * Gets a new number which is the difference of instance and argument.
     * @param  \BN\INumber $number
     * @return \BN\INumber
     */
    public function subtract(INumber $number);

    /**
     * Gets a new number which is the product of instance and argument.
     * @param  \BN\INumber $number
     * @return \BN\INumber
     */
    public function multiply(INumber $number);

    /**
     * Gets a new number which is the division of instance and argument.
     * @example 5 / 2 = 2.5
     * @param  \BN\INumber               $number
     * @return \BN\INumber
     * @throws \InvalidArgumentException if $number equals 0
     */
    public function divide(INumber $number);

    /**
     * Gets a new number which is the division of instance and argument without
     * remainder.
     * @example 10 \ 3 = 3
     * @example 10 \ -3 = -3
     * @example -10 \ 3 = -3
     * @example -10 \ -3 = 3
     * @return \BN\INumber
     * @throws \InvalidArgumentException if $number equals 0
     */
    public function quotient(INumber $number);

    /**
     * Gets a new number which is the remainder after division of instance by argument.
     * It supports a decimal remainder (calculated m(x,n) = x - n * floor(x/n)).
     * See the rules in examples:
     * @example 10 % 2.1 = 1.6
     * @example 10 % -2.1 = 1.6
     * @example -10 % 2.1 = -1.6
     * @example -10 % 2.1 = -1.6
     * @param  \BN\INumber               $number
     * @return \BN\INumber
     * @throws \InvalidArgumentException if $number equals 0
     */
    public function modulo(INumber $number);

    /**
     * Gets a new number which is instance raised to the power of argument.
     * If power is decimal number and it's not square root (1/2) then numbers
     * are typed to float (IEEE 754) and then pow function is used. Result can
     * be influenced by inaccurate floating point precision.
     * @param  \BN\INumber $number
     * @return \BN\INumber
     */
    public function power(INumber $number);

    /**
     * Gets a new number which is the square root of instance.
     * @return \BN\INumber
     * @throws \InvalidArgumentException if $number is smaller than 0
     */
    public function sqrt();

    /**
     * Gets a new number which is the absolute value of instance.
     * @return \BN\INumber
     */
    public function abs();

    /**
     * Gets a new number which is the negated instance.
     * @return \BN\INumber
     */
    public function negate();

    /**
     * Returns true if instance is integer. Number is integer if don't have a
     * decimal point or if numbers after decimal points are zeros.
     * @param  \BN\INumber $number
     * @return boolean
     */
    public function isInteger();

    /**
     * Returns true if instance equal to zero.
     * @param  \BN\INumber $number
     * @return boolean
     */
    public function isZero();

    /**
     * Returns true if instance is smaller than zero.
     * @param  \BN\INumber $number
     * @return boolean
     */
    public function isNegative();

    /**
     * Returns true if instance is larger than zero.
     * @param  \BN\INumber $number
     * @return boolean
     */
    public function isPositive();

    /**
     * Compares instance with argument and returns 0 if number are equal. If
     * instance is larger than argument it returns 1. Otherwise returns -1.
     * @param  \BN\INumber $number
     * @return int
     */
    public function compare(INumber $number);

    /**
     * Returns true if instance and argument are equal numbers.
     * @param  \BN\INumber $number
     * @return boolean
     */
    public function isEqual(INumber $number);

    /**
     * Returns true if instance is larger than argument
     * @param  \BN\INumber $number
     * @return boolean
     */
    public function isBiggerThan(INumber $number);

    /**
     * Returns true if instance is less than argument.
     * @param  \BN\INumber $number
     * @return boolean
     */
    public function isSmallerThan(INumber $number);

    /**
     * Gets a new number which is number rounded to nearest number. Precision is
     * number of digits before (negative) or after (positive number) the decimal point.
     * @example 2.56 round 1 = 2.6
     * @example 74 round -1 = 70
     * @param  int                       $precision
     * @return \BN\INumber
     * @throws \InvalidArgumentException if $precision is not integer
     */
    public function round($precision);

    /**
     * Gets a new number which is the next integer farthest from zero.
     * It round fractions up.
     * @example 8.62 roundUp = 9
     * @example -3.2 roundUp = -4
     * @return \BN\INumber
     */
    public function roundUp();

    /**
     * Gets a new number which is the the next integer closest to zero.
     * It round fractions down.
     * @example 8.62 roundDown = 8
     * @example -3.2 roundDown = -3
     * @return \BN\INumber
     */
    public function roundDown();

    /**
     * Gets a new number which is the nearest multiple of precision. If precision
     * is positive number (round after decimal point) then it at first rounds number
     * to number of digits in precision.
     * @example 8.6256 roundTo 20 (= temporary 8.63 roundTo 20) = 8.60
     * @example 165 roundTo -50 = 150
     * @param  int                       $precision
     * @return \BN\INumber
     * @throws \InvalidArgumentException if $precision is not integer
     */
    public function roundToNumber($precision);

    /**
     * Sets a local scale used to set number of digits after decimal point in
     * the result. It's defined only for the instance, all other instances will
     * use global scale. Be careful about using local scale for methods modulo
     * and roundToNumber, because they are creating temporary numbers without
     * local scale.
     * @param  int                       $scale
     * @throws \InvalidArgumentException if $scale is not integer or it's smaller than 2
     */
    public function setLocalScale($scale);

    /**
     * Operation won't use local scale, but global scale (if it's defined).
     * E.g. in BC Math global scale is defined by function bcscale.
     */
    public function resetLocalScale();

    /**
     * Gets a string represetation of number.
     * @return string
     */
    public function __toString();

    /**
     * Clones the current instance.
     * @return INumber
     */
    public function __clone();
}


function synchronizeSelectAndTextarea() {
    $('#examples').change(function() {
        var option = $(this).find("option:selected");
        var input = option.attr('data-input');
        $('textarea').html(input);
    });
}

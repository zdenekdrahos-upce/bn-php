<?php

/**
 * Register autoload function for BN-PHP
 */
spl_autoload_register('bnPhpAutoload');

function bnPhpAutoload($class)
{
    if (strpos($class, 'BN\\') === 0) {
        $classPath = str_replace('\\', DIRECTORY_SEPARATOR, $class);
        require_once(__DIR__ . "/../lib/{$classPath}.php");
    }
}

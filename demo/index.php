<?php
/** Set default scale parameter for all BCMath functions */
bcscale(20);

require_once __DIR__ . '/app/bootstrap.php';
require_once __DIR__ . '/app/examples.php';

$grammar = \Demo\grammar();
$benchmark = new \Demo\Benchmark();
$controller = new \Demo\Controller($grammar, $_POST);
$benchmark->benchmark('bn-php', $controller);
$model = $controller->getModel();
$examples = \Demo\examples();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>BN-PHP DEMO</title>
        <link rel="stylesheet" href="assets/screen.css" type="text/css" />
        <script src="assets/jquery.1.11.0.min.js"></script>
        <script src="assets/scripts.js"></script>
        <script>
            $(function() {
                synchronizeSelectAndTextarea();
            });
        </script>
    </head>
    <body>

        <header>
            <h1>
                <a href="">
                    <img src="https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2013/Jan/29/bn-php-logo-966140309-0_avatar.png" alt="BN-PHP" />
                </a>
            </h1>
        </header>

        <section>
            <h2>Grammar</h2>
            <strong>Language</strong> <?php echo implode(' &nbsp; ', $grammar->keywordsLanguage); ?>
            <strong>Constants</strong> <?php echo implode(' &nbsp; ', $grammar->keywordsConstants); ?>
        </section>

        <section>
            <h2>Input</h2>
            <form method="POST" action="./">
                <label>
                    <span>Examples:</span>
                    <select id="examples">
                        <option data-input=""></option>
                        <?php
                        foreach ($examples as $group => $expressions) {
                            echo "<optgroup label='{$group}'>";
                            foreach ($expressions as $name => $input) {
                                $selected = $input == $model->input ? 'selected' : '';
                                echo "<option data-input='{$input}' {$selected}>{$name}</option>";
                            }
                            echo "</optgroup>";
                        }
                        ?>
                    </select>
                </label>
                <label>
                    <span>Expression:</span>
                    <textarea name="input" rows="5"><?php echo $model->input; ?></textarea>
                </label>
                <input type="submit" value="Calculate!">
            </form>
        </section>

        <div id="comparison">
            <section>
                <h2>BN-PHP</h2>
                <h3>Result</h3>
                <strong class="wrap"><?php echo $model->result; ?></strong>

                <?php if ($model->variables): ?>
                <h3>Variables</h3>
                <pre class="wrap"><?php echo implode("\n", $model->variables); ?></pre>
                <?php endif; ?>

                <?php if ($model->error): ?>
                <h3>Error log</h3>
                <pre class="wrap"><?php echo $model->error; ?></pre>
                <?php endif; ?>

                <h3>Benchmark</h3>
                <strong><?php echo $benchmark->getTime('bn-php'); ?></strong>
            </section>

            <section>
                <h2>Native PHP eval</h2>
                <h3>Result</h3>
                <?php
                $eval = new \Demo\NativeEval($model->input);
                $benchmark->benchmark('eval', $eval);
                ?>
                <strong><?php echo $eval->result; ?></strong>
                <h3>Benchmark</h3>
                <strong><?php echo $benchmark->getTime('eval'); ?></strong>
                <h3>PHP code</h3>
                <pre><?php echo $eval->input; ?></pre>
            </section>
        </div>

        <footer>
            <span><a href="https://bitbucket.org/zdenekdrahos/bn-php/src/tip/license.txt" target="_blank">New BSD license</a></span>
            <span>Source codes are available at <a href="https://bitbucket.org/zdenekdrahos/bn-php">Bitbucket.org</a></span>
            <span>Created by <a href="http://zdenekdrahos.bitbucket.org/">Zdeněk Drahoš</a></span>
        </footer>
        
    </body>
</html>

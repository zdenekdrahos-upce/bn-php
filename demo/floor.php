<?php
bcscale(1);
require_once __DIR__ . '/app/bootstrap.php';

$benchmark = new \Demo\Benchmark();
$benchmark->benchmark('number', function() {
    $a = new BN\Number('0.1');
    $b = new BN\Number('0.7');
    $ten = new BN\Number('10');
    echo 'floor((0.1+0.7)*10) = ' . $a->add($b)->multiply($ten)->roundDown();
});

echo "<br />{$benchmark->getTime('number')}";

<?php
require_once __DIR__ . '/../../demo/autoload.php';
require_once __DIR__ . '/../../tests/helpers/IntegerOperators.php';

require_once __DIR__ . '/grammar.php';
require_once __DIR__ . '/examples.php';

require_once __DIR__ . '/Controller.php';
require_once __DIR__ . '/Presenter.php';
require_once __DIR__ . '/Model.php';

require_once __DIR__ . '/benchmark.php';
require_once __DIR__ . '/eval.php';

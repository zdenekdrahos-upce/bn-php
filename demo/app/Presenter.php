<?php

namespace Demo;

use \BN\Compiler\EvaluatorResponder;
use BN\Compiler\Postfix\Operands\OperandsSummary;

class Presenter implements EvaluatorResponder
{
    private $model;
    private $statementsCount;

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->statementsCount = 0;
    }

    public function nextStatement($statement)
    {
        $this->statementsCount++;
    }

    public function unknownToken($token)
    {
        $this->addError("Unknown lexeme <strong>{$token}</strong>");
    }

    public function unknownOperator($operator)
    {
        $this->addError("Undefined operator <strong>{$operator}</strong>");
    }

    public function mismatchedBrackets($mismatchedBracket)
    {
        $this->addError("Mismatched bracket <strong>{$mismatchedBracket}</strong>");
    }

    public function invalidOperands($operatorSymbol, OperandsSummary $operands)
    {
        $this->addError(
            "Operator <strong>{$operatorSymbol}</strong> must have <strong>{$operands->expectedCount}</strong>, " .
            "but had <strong>{$operands->countOperands()}</strong> operands"
        );
    }

    public function missingOperator($expectedOperandsCount)
    {
        $this->addError("Missing operator with <strong>{$expectedOperandsCount}</strong> operands");
    }

    public function undefinedVariable($variableName)
    {
        $this->addError("Undefined variable <strong>{$variableName}</strong>");
    }

    public function exception(\Exception $e)
    {
        $class = get_class($e);
        $this->addError(
            "<strong>{$class}</strong> in file <strong>{$e->getFile()}</strong> " .
            "on line <strong>{$e->getLine()}</strong>"
        );
    }

    private function addError($errorMessage)
    {
        $this->model->error .= "<em>{$this->statementsCount}:</em> {$errorMessage}\n";
    }

    public function result($result)
    {
        $this->model->result = $result;
    }

    public function variables(\stdClass $variables)
    {
        foreach (get_object_vars($variables) as $name => $value) {
            $this->model->variables[] = "\${$name} = {$value}";
        }
    }
}

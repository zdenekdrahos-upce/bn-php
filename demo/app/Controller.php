<?php

namespace Demo;

use BN\Compiler\Grammar\GrammarBuilder;
use BN\Compiler\EvaluatorInteractor;

class Controller
{
    private $model;
    private $interactor;

    public function __construct(GrammarBuilder $grammar, $data)
    {
        $input = isset($data['input']) ? $data['input'] : '';
        $this->model = new Model($input);
        $this->interactor = new EvaluatorInteractor($grammar);
    }

    public function __invoke()
    {
        $variables = new \stdClass();
        $presenter = new Presenter($this->model);
        $this->interactor->__invoke($this->model->input, $variables, $presenter);
        $presenter->variables($variables);
    }

    public function getModel()
    {
        return $this->model;
    }
}

<?php

namespace Demo;

class NativeEval
{
    public $input;
    public $result;

    public function __construct($input)
    {
        $semicolonsCount = substr_count($input, ';');
        if ($input === '') {
            $this->input = 'return 0;';
        } elseif ($semicolonsCount == 0) {
            $this->input = "return {$input};";
        } elseif ($semicolonsCount == 1) {
            $this->input = "return {$input}";
        } else {
            $lastSemicolon = strrpos($input, ';', -2) + 1;
            $before = substr($input, 0, $lastSemicolon);
            $after = substr($input, $lastSemicolon);
            $this->input = "{$before} return $after";
        }
    }

    public function __invoke()
    {
        $result = @eval($this->input);
        $this->result = $result !== false && $result !== null ? $result : 'Invalid PHP code';
    }
}

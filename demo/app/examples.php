<?php

namespace Demo;

function examples()
{
    return array(
        'Basic' => array(
            'Adddition' => '1+2+3',
            'Power' => '2^3',
            'Brackets' => '(1+2)*3',
            'Aggregate' => 'sum(1,2,3,2, 1) + 2*3',
            'Aggregate & precedence' => 'sum(1 + 2 3 + 2 1 * 1) + 2*3',
        ),
        'BN examples' => array(
            'Floating point precision' => 'floor((0.1+0.7)*10)',
            'Modulo' => expressionsToLines(
                '$a = 10 % 2.1',
                '$b = 10 % -2.1',
                '$c = -10 % 2.1',
                '$d = -10 % -2.1'
            ),
            'Precedence' => '-2+5)^(5+5)*5+2-2+-2)',
            'Power' => '256 ^ (1 / 8)',
            'Negate & quotient' => 'neg (5 \ 2)',
            'Round' => expressionsToLines(
                '$number = 123456789.987654321',
                '$a = round($number, 5)',
                '$b = round($number, 2)',
                '$c = round($number, 0)',
                '$d = round($number, -2)',
                '$e = round($number, -5)'
            ),
            'Round 0.000055 to number' => expressionsToLines(
                '$number = 0.000055',
                '$a = $number round 3',
                '$b = $number round 4',
                '$c = $number round 5',
                '$d = $number roundTo 10',
                '$e = $number roundTo 40',
                '$f = $number roundTo 100'
            ),
            'Round 124 to number' => expressionsToLines(
                '$number = 124',
                '$a = $number roundTo -10',
                '$b = $number roundTo -50',
                '$c = $number roundTo -150',
                '$d = $number roundTo -500'
            ),
            'Round & round to number' => expressionsToLines(
                '$number = 0-12499',
                '$a = $number round -2',
                '$b = $number roundTo -100'
            ),
            'Integration test' => '(-2+5)^(5+5)*5+2-2+-2'
        ),
        'Variables, Constants' => array(
            'Undefined variable' => '$var + 5',
            'Assign variable' => '$a = 5 * 2',
            'Constant PI' => 'PI',
            'Constant e' => 'e',
            'Sum available constants' => 'sum(PI,π,e)',
            'Circumference - variable & constant' => '$r=1;' . "\n" . '$circumference = 2*π*$r;',
        ),
        'Errors' => array(
            'Parser errors' => expressionsToLines('1 & 2', '(', ')'),
            'Postfix errors' => expressionsToLines('1 +', '1 2', 'sum')
        )
    );
}

function expressionsToLines()
{
    return implode(";\n", func_get_args()) . ';';
}

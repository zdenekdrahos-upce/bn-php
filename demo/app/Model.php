<?php

namespace Demo;

class Model
{
    public $input = '';
    public $result = 0;
    public $error = '';
    public $variables = array();

    public function __construct($input)
    {
        $this->input = $input;
    }
}

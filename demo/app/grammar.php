<?php

namespace Demo;

use BN\Compiler\Grammar\GrammarBuilder;
use BN\OperatorsFactory;
use BN\NumberFactory;
use BN\Compiler\Scanner\Converter\LexemeToBNNumber;

function grammar()
{
    $numbers = new NumberFactory();
    $operators = new OperatorsFactory();
    $toNumber = new LexemeToBNNumber();
    $grammar = new GrammarDecorator($toNumber);
    return $grammar
        ->statementsSeparator(';')
        ->whiteSpace("\n", "\r", "\n\r", "\t", ',')
        ->assign('=')
        ->brackets('(', ')')
        ->unarySigns('-', '+')
        ->numberConstant('PI', $numbers->createPI())
        ->numberConstant('π', $numbers->createPI())
        ->numberConstant('e', $numbers->createEulerNumber())
        ->operators(
            $operators->getAll()
        );
}

class GrammarDecorator extends GrammarBuilder
{
    public $keywordsLanguage = array();
    public $keywordsConstants = array();

    public function statementsSeparator($statementsSeparator)
    {
        $this->keywordsLanguage[] = $statementsSeparator;
        return parent::statementsSeparator($statementsSeparator);
    }

    public function assign($assignOperator)
    {
        $this->keywordsLanguage[] = $assignOperator;
        return parent::assign($assignOperator);
    }

    public function keyword($symbol, $tokenType)
    {
        $this->keywordsLanguage[] = $symbol;
        return parent::keyword($symbol, $tokenType);
    }

    public function numberConstant($symbol, $value)
    {
        $this->keywordsConstants[] = $symbol;
        return parent::numberConstant($symbol, $value);
    }
}

<?php

namespace Demo;

class Benchmark
{
    private $durations;

    public function benchmark($name, $function)
    {
        $start = microtime(true);
        $function();
        $end = microtime(true);
        $this->durations[$name] = $end - $start;
    }

    public function getTime($name)
    {
        return $this->durations[$name];
    }
}
